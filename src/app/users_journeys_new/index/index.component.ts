import {Component, OnInit} from '@angular/core';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {Restangular} from 'ngx-restangular';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {ResponseContentType} from "@angular/http";

@Component({
    selector: 'app-index',
    templateUrl: './index.component.html',
    styleUrls: ['./index.component.css']
})
export class IndexComponent implements OnInit {

    rows: Array<any>;
    rows1: Array<any>;
    users: Array<any>;
    jurneysArray:  Array<any>;
    JurneyType : any = 1;
    selectedImage;
    TableWidth:any = window.innerWidth *0.75;
    imageModal: any;
    points: Array<any>;
    deleteModal: any;
    itemToDelete: any;
    editMode: boolean = false;
    editRow: any = null;
    fieldsToEdit = {name: '', phone: ''};
    createModal: any;
    showDetails:any = 1;
    flag = 0;
    filteredArray = [];
    filteredArray2 = [];

    form: FormGroup = this.fb.group({
        name: new FormControl('', Validators.required),
        email: new FormControl('', Validators.required),
        phone: new FormControl('', Validators.required),
    });

    constructor(public restangular: Restangular, public modalService: NgbModal, public fb: FormBuilder) { }

    async ngOnInit() {
        // this.rows = await this.restangular.all('getUsersByJourneyNew').getList().toPromise();
        this.getJurneyNames();
        this.getData(this.JurneyType);

    }

    async getJurneyNames() {
        this.jurneysArray = await this.restangular.all('getJurneyNames').getList().toPromise();
        //console.log ("jurneysArray",this.jurneysArray)
    }
    
    async DeleteJourney(row,index)
    {
         console.log("Row " , row.id);

        let confirmBox = confirm("Are you sure you want to delete this journey?");
        if (confirmBox){
            let payload = {JurneyId: row.id};
            await this.restangular.all('DeleteJourney','').customPOST(payload).toPromise();
            await this.getJurneyNames();
            await this.getData(this.JurneyType);
        }
    }

    async getData(type)
    {
        let data = new FormData();
        data.append('JurneyType', type);
         this.rows = await this.restangular.all('getUsersByJourneyNew').customPOST(data).toPromise();
         this.rows = this.filterArray(this.rows);
         //this.rows = this.SortArray(this.rows);
      
        // this.rows = await this.restangular.all('getUsersByJourneyNew2').customPOST(data).toPromise();
        this.rows1 = this.rows;//await this.restangular.all('getUsersByJourneyNew').customPOST(data).toPromise();
        console.log("TouchPoints : " , this.rows );
        // for (let i = 0; i < this.rows.length; i++) {
        //     this.rows[i].isHidden = true;
        //     for (let g = 0; g < this.rows[i].touchpoints.length; g++) {
        //     }
        // }
    }

    ChangeJurneyType(type) {
        this.JurneyType = type;
        this.getData(this.JurneyType);
    }


    showRow(row,i) {
        console.log("rowType1 " , row.isHidden)
        // if(this.rows[i].isHidden )
        // {
        //     this.rows[i].isHidden = false;
        // }
        
        if (row.isHidden == true )
            row.isHidden = false;
        else if (row.isHidden == false )
            row.isHidden = true
        
        console.log("rowType2 " , row)
    }

    exportExcel(locale) {
        window.open("http://whiteboard.kartisim.co.il/v1/getUsersByJourneyExcel?id="+this.JurneyType+"&locale="+locale,"_self");
    }

    openImageModal (content, image) {
        this.selectedImage = image;
        this.imageModal = this.modalService.open(content);
    }

    updateFilter(event) {
        const val = event.target.value.toLowerCase();
        // filter our data
        this.rows = this.rows1;
        const temp = this.rows.filter(function (d) {
            return d.user_data.email && d.user_data.email.toLowerCase().indexOf(val) !== -1
        });
        // update the rows
        this.rows = temp;
    }

    async deleteItem(){
        await this.itemToDelete.remove().toPromise();
        this.deleteModal.close();
        this.rows = await this.restangular.all('users').getList().toPromise();
        this.users = this.rows;
    }

    openDeleteModal(content, item) {
        this.deleteModal = this.modalService.open(content);
        this.itemToDelete = item;
    }

    switchToEditMode (item) {
        this.editMode = true;
        this.editRow = item;
        this.fieldsToEdit.name = item.name;
        this.fieldsToEdit.phone = item.phone;
    }

    async editItem () {
        this.editRow.name = this.fieldsToEdit.name;
        this.editRow.phone = this.fieldsToEdit.phone;
        await this.editRow.patch().toPromise();
        this.rows = await this.restangular.all('users').getList().toPromise();
        this.users = this.rows;
        this.cancelEdit();
    }

    cancelEdit () {
        this.editMode = false;
        this.ngOnInit();
    }


    openCreateModal (content) {
        this.createModal = this.modalService.open(content);
    }

    async createItem (){
        let item = this.restangular.restangularizeElement('', {name: this.form.value.name, email: this.form.value.email, phone: this.form.value.phone}, 'users');
        await item.save().toPromise();
        this.createModal.close();
        this.rows = await this.restangular.all('users').getList().toPromise();
        this.users = this.rows;
    }
    
    
    showDetailsFun()
    {
        console.log(this.showDetails);
        if(this.showDetails == 1)
            this.showDetails = 0 ;
        else
            this.showDetails = 1;
    }
    
    filterArray(arr)
    {
        
        var i=0;

       
        for(let item of arr){
            this.filteredArray = [];
            for( let tPoints of item.touchpoints)
            {
                this.flag = 0;
            
                for(let touchPoint of this.filteredArray)
                {
                    //console.log("Item1 " , tPoints.title , touchPoint.title)
                    if(tPoints.point_id == touchPoint.point_id)
                    {
                        this.flag = 1;
                        //addContent
                        if(!touchPoint.media)
                            touchPoint.media = [];
                    
                        if(tPoints.media)
                        {
                            for(let media of tPoints.media)
                                touchPoint.media.push(media);
                        }
                    
                        if(!touchPoint.texts)
                            touchPoint.texts = [];
                    
                        if(tPoints.texts)
                        {
                            for(let text of tPoints.texts)
                                touchPoint.texts.push(text);
                        }
    
                        if(!touchPoint.texts)
                            touchPoint.texts = [];
    
                        if(tPoints.mark)
                            touchPoint.mark = tPoints.mark;
                    }
                }
            
                if(this.flag == 0)
                    this.filteredArray.push(tPoints);
            }
        
            item.touchpoints = this.filteredArray;
        }


        arr = this.SortArray(arr)

        return arr;


    }
    
    
    SortArray(arr) {
    
        for (let point of arr) {
            point.touchpoints.sort((a, b): number => {
                if (a.weight < b.weight) return -1;
                else if (a.weight > b.weight) return 1;
                else return 0;
            });
        }
        
        return arr;
    }
    
    getUrlLenth(url)
    {
        if(url.endsWith("3gp"))
        {
            console.log("End1",url)
            return 1;
        }
        else {
            console.log("End2", url)
            return 0;
        }
    }


}
