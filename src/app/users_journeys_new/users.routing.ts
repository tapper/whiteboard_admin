import {Routes} from '@angular/router';
import {IndexComponent} from './index/index.component';

export const UsersRoutes: Routes = [{
    path: '',
    children: [
        {
            path: '',
            component: IndexComponent,
            data: {heading: 'User Journeys New'},
        }
    ]
}];
