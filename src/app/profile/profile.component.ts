import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {Restangular} from 'ngx-restangular';

@Component({
    selector: 'app-profile',
    templateUrl: './profile.component.html',
    styleUrls: ['./profile.component.css']
})
export class IndexComponent implements OnInit {

    saved: boolean = false;
    form1: FormGroup = this.fb.group({
        username: new FormControl('', Validators.required),
        password: new FormControl('', Validators.required),
        email: new FormControl('', [Validators.required, Validators.email]),
    });
    form2: FormGroup = this.fb.group({
        password: new FormControl('', Validators.required),
    });
    admin: any;
    password: any;

    constructor(public fb: FormBuilder, public restangular: Restangular) {}

    async ngOnInit() {
        this.admin = await this.restangular.one('admins', localStorage.id).get().toPromise();
        this.form1.setValue({username: this.admin.username, password: this.admin.password, email: this.admin.email});
        this.password = await this.restangular.all('passwords/last').customGET().toPromise();
        this.form2.setValue({password: this.password.password});
    }

    async onSubmit1 () {
        this.saved = false;
        await this.restangular.one('admins', this.admin.id).customPOST({username: this.form1.value.username, password: this.form1.value.password, email: this.form1.value.email}).toPromise();
        this.saved = true;
        this.admin = await this.restangular.one('admins', localStorage.id).get().toPromise();
        this.form1.setValue({username: this.admin.username, password: this.admin.password, email: this.admin.email});
    }

    async onSubmit2 () {
        this.saved = false;
        await this.restangular.all('passwords').customPOST({password: this.form2.value.password}).toPromise();
        this.saved = true;
        this.password = await this.restangular.all('passwords/last').customGET().toPromise();
        this.form2.setValue({password: this.password.password});
    }

}
