import {Routes} from '@angular/router';

import {AdminLayoutComponent} from './layouts/admin/admin-layout.component';
import {AuthLayoutComponent} from './layouts/auth/auth-layout.component';
import {AuthGuard} from './_guards/auth.guard';

export const AppRoutes: Routes = [{
    path: '',
    component: AdminLayoutComponent,
    children: [
        {
            path: '',
            loadChildren: './dashboard/dashboard.module#DashboardModule'
        },
        {
            path: 'profile',
            loadChildren: './profile/profile.module#ProfileModule'
        },
        {
            path: 'points',
            loadChildren: './points/points.module#PointsModule'
        },
        {
            path: 'journeys',
            loadChildren: './journeys/journeys.module#JourneysModule'
        },
        {
            path: 'users',
            loadChildren: './users/users.module#UsersModule'
        },
        {
            path: 'users/:id/journeys',
            loadChildren: './user_journeys/user_journeys.module#UserJourneysModule'
        },
        {
            path: 'email',
            loadChildren: './email/email.module#EmailModule'
        },
        {
            path: 'user_journeys_excel',
            loadChildren: './user_journeys_excel/journeys.module#JourneysModule'
        },{
            path: 'users_journeys_new',
            loadChildren: './users_journeys_new/users.module#UsersModule'
        }],
    canActivate: [AuthGuard]
}, {
    path: '',
    component: AuthLayoutComponent,
    children: [
        {
            path: 'authentication',
            loadChildren: './authentication/authentication.module#AuthenticationModule'
        }, {
            path: 'error',
            loadChildren: './error/error.module#ErrorModule'
        }]
}, {
    path: '**',
    redirectTo: 'error/404'
}];

