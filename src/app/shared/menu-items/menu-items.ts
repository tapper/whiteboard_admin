import {Injectable} from '@angular/core';

export interface BadgeItem {
    type: string;
    value: string;
}

export interface ChildrenItems {
    state: string;
    name: string;
    type?: string;
}

export interface Menu {
    state: string;
    name: string;
    type: string;
    icon: string;
    badge?: BadgeItem[];
    children?: ChildrenItems[];
}

const MENUITEMS = [
    {
        state: '/',
        name: 'HOME',
        type: 'link',
        icon: 'basic-accelerator'
    },
    {
        state: 'users',
        name: 'USERS',
        type: 'link',
        icon: 'basic-picture-multiple'
    },
    {
        state: 'journeys',
        name: 'JOURNEYS',
        type: 'link',
        icon: 'basic-book-pen'
    },
    {
        state: 'points',
        name: 'POINTS',
        type: 'link',
        icon: 'basic-star'
    },
    {
        state: 'user_journeys_excel',
        name: 'User Journeys',
        type: 'link',
        icon: 'basic-star'
    },
    {
        state: 'users_journeys_new',
        name: 'User Journeys New',
        type: 'link',
        icon: 'basic-star'
    },
];

@Injectable()
export class MenuItems {
    getAll(): Menu[] {
        return MENUITEMS;
    }

    add(menu: Menu) {
        MENUITEMS.push(menu);
    }
}
