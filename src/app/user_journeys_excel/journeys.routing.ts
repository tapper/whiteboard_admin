import {Routes} from '@angular/router';
import {IndexComponent} from './index/index.component';

export const JourneysRoutes: Routes = [{
    path: '',
    children: [
        {
            path: '',
            component: IndexComponent,
            data: {heading: 'User Excel Journeys'},
        },

    ]
}];
