import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
    name: 'groupByPipe'
})

export class groupByPipe implements PipeTransform {
    filteredArray = [];
    i=0;
    flag = 0;
    
     transform(arr: any): any {
         for(let item of arr){
             this.filteredArray = [];
             for( let tPoints of item.touchpoints)
             {
                 this.flag = 0;
                 
                 for(let touchPoint of this.filteredArray)
                 {
                     //console.log("Item1 " , tPoints.title , touchPoint.title)
                     if(tPoints.title == touchPoint.title)
                     {
                         this.flag = 1;
                         //addContent
                         if(!touchPoint.media)
                             touchPoint.media = [];
                         
                         if(tPoints.media)
                         {
                             for(let media of tPoints.media)
                                 touchPoint.media.push(media);
                         }
                    
                         if(!touchPoint.texts)
                             touchPoint.texts = [];
                         
                         if(tPoints.texts)
                         {
                             for(let text of tPoints.texts)
                                touchPoint.texts.push(text);
                         }
                     }
                 }
    
                 if(this.flag == 0)
                     this.filteredArray.push(tPoints);
             }
    
             item.touchpoints = this.filteredArray;
       }
       return arr;
     }
    
}


// ///for(let item of arr){
// let i=0;
// this.filteredArray = [];
// for( let tPoints of item.touchpoints)
// {
//     let flag = 0;
//
//     for(let touchPoint of this.filteredArray)
//     {
//         if(touchPoint.point_id == tPoints.point_id)
//         {
//             // add content ;
//             //console.log("Flag = 1 " , touchPoint.point_id)
//             flag = 1;
//         }
//     }
//
//     if(flag == 0) this.filteredArray.push(tPoints);
// }
// console.log(i , this.filteredArray.length)
// arr[i].touchpoints = this.filteredArray
// i++;
// }