import {CommonModule} from '@angular/common';
import {RouterModule} from '@angular/router';
import {NgModule} from '@angular/core';
import {NgxDatatableModule} from '@swimlane/ngx-datatable';
import {IndexComponent} from './index/index.component';

import {FileUploadModule} from 'ng2-file-upload/ng2-file-upload';
import {TreeModule} from 'angular-tree-component';
import {CustomFormsModule} from 'ng2-validation';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {NgbProgressbarModule} from '@ng-bootstrap/ng-bootstrap';
import {TextMaskModule} from 'angular2-text-mask';
import {LoadingModule} from 'ngx-loading';
import {CreateComponent} from './create/create.component';
import {EditComponent} from './edit/edit.component';
import {AngularMultiSelectModule} from 'angular2-multiselect-dropdown';
import {MomentModule} from 'angular2-moment';
import {JourneysRoutes} from './journeys.routing';
import {SortablejsModule} from 'angular-sortablejs';

@NgModule({
    imports: [CommonModule,
        RouterModule.forChild(JourneysRoutes),
        NgxDatatableModule,
        FormsModule,
        ReactiveFormsModule,
        NgbProgressbarModule,
        CustomFormsModule,
        TreeModule,
        FileUploadModule,
        LoadingModule,
        TextMaskModule,
        AngularMultiSelectModule,
        MomentModule,
        SortablejsModule,
    ],
    declarations: [IndexComponent, CreateComponent, EditComponent]
})

export class JourneysModule {}