import {ChangeDetectionStrategy, Component, OnInit} from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {ActivatedRoute, Router} from '@angular/router';
import {DomSanitizer} from '@angular/platform-browser';
import {Restangular} from 'ngx-restangular';

@Component({
    selector: 'app-edit',
    changeDetection: ChangeDetectionStrategy.Default,
    templateUrl: './edit.component.html',
    styleUrls: ['./edit.component.css'],
})
export class EditComponent implements OnInit {

    form: FormGroup = this.fb.group({
        title_en: new FormControl('', Validators.required),
        title_he: new FormControl('', Validators.required),
        points: new FormControl([], Validators.required),
    });
    logo = {path: null, file: null};
    logo2 = {path: null, file: null};
    journey;
    errors: Array<any> = [];
    points: Array<any> = [];
    dropdownSettings = {
        singleSelection: false,
        text:"Select points",
        selectAllText:'Select All',
        unSelectAllText:'UnSelect All'
    };

    constructor(public fb: FormBuilder,
                public activatedRoute: ActivatedRoute,
                private router: Router,
                private sanitizer: DomSanitizer,
                public restangular: Restangular) {}

    async ngOnInit() {
        this.points = await this.restangular.all('points').getList().toPromise();
        this.activatedRoute.params.subscribe(async (data) => {
            this.journey = await this.restangular.one('journeys', data.id).get().toPromise();
            console.log("points:",this.journey.points);
            this.form.setValue({title_en: this.journey.title_en, title_he: this.journey.title_he, points: this.journey.points});
        });
    }

    onFileChange(event, type) {
        let file = event.target.files[0];
        console.log ("file",file);
        let reader = new FileReader();

        reader.onload = ev => {
            if (type === 'logo'){
                this.logo.path = this.sanitizer.bypassSecurityTrustResourceUrl((<any>ev.target).result);
                this.logo.file =  file;
            }
            else if (type === 'logo2'){
                this.logo2.path = this.sanitizer.bypassSecurityTrustResourceUrl((<any>ev.target).result);
                this.logo2.file =  file;
            }
        };
        reader.readAsDataURL(file);
    }

    async onSubmit() {

        this.errors = [];

        if (this.journey.image == '' && this.logo.file === null){
            this.errors.push({message: "Picture is empty!"});
            return;
        }

        await this.restangular.one('journeys', this.journey.id).customPATCH({title_en: this.form.value.title_en, title_he: this.form.value.title_he, points: this.form.value.points}).toPromise();

        if (this.logo.file){
            let fdLogo = new FormData();
            fdLogo.append('journey_id', this.journey.id);
            fdLogo.append('file', this.logo.file);
            fdLogo.append('imagenumber', "1");

            await this.restangular.all('media').customPOST(fdLogo).toPromise();
        }


        if (this.logo2.file){
            let fdLogo = new FormData();
            fdLogo.append('journey_id', this.journey.id);
            fdLogo.append('file', this.logo2.file);
            fdLogo.append('imagenumber', "2");
            await this.restangular.all('media').customPOST(fdLogo).toPromise();
        }

        this.router.navigate(['journeys']);

    }

}