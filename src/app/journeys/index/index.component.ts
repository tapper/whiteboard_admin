import {Component, OnInit} from '@angular/core';
import {ApiService} from '../../_services/api/api.service';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {Restangular} from 'ngx-restangular';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';

@Component({
    selector: 'app-index',
    templateUrl: './index.component.html',
    styleUrls: ['./index.component.css']
})
export class IndexComponent implements OnInit {

    rows: Array<any>;
    journeys: Array<any>;
    // points: Array<any>;
    deleteModal: any;
    itemToDelete: any;
    form: FormGroup = this.fb.group({title: new FormControl('', Validators.required),});
    dropdownSettings = {
        text:"Select points",
        disabled: true
    };

    options: Array<any> = [];

    constructor(public restangular: Restangular, public modalService: NgbModal, public fb: FormBuilder) { }

    async ngOnInit() {
        this.rows = await this.restangular.all('journeys').getList().toPromise();
        this.journeys = this.rows;
        console.log("journeys",this.journeys);

        for (let journey of this.journeys) {
            this.options[journey.id] = {
                group: journey.id,
                onUpdate: async (event: any) => {
                    await this.restangular.one('journeys', journey.id).customPATCH({
                        order: this.journeys.find(j => j.id == journey.id).points.map(p => p.id),
                    }).toPromise();
                }
            }
        }
    }

    updateFilter(event) {
        const val = event.target.value.toLowerCase();
        const temp = this.journeys.filter(function(d) {
            return d.title_en && d.title_en.toLowerCase().indexOf(val) !== -1 || d.title_he && d.title_he.toLowerCase().indexOf(val) !== -1 || !val;
        });
        this.rows = temp;
    }

    async deleteItem(){
        await this.itemToDelete.remove().toPromise();
        this.deleteModal.close();
        this.rows = await this.restangular.all('journeys').getList().toPromise();
        this.journeys = this.rows;
    }

    openDeleteModal(content, item) {
        this.deleteModal = this.modalService.open(content);
        this.itemToDelete = item;
    }

}
