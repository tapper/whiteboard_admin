import {ChangeDetectionStrategy, Component, OnInit} from '@angular/core';
import {FormBuilder} from '@angular/forms';
import {ActivatedRoute, Router} from '@angular/router';
import {Restangular} from 'ngx-restangular';

@Component({
    selector: 'app-index',
    changeDetection: ChangeDetectionStrategy.Default,
    templateUrl: './index.component.html',
    styleUrls: ['./index.component.css'],
})
export class IndexComponent implements OnInit {

    user: any;
    journeys: Array<any> = [];
    rows: Array<any> = [];
    user_journeys: Array<any> = [];
    i:number=0;
    spliceArray: Array<any> = [];
    
    constructor(public fb: FormBuilder,
                private router: Router,
                public activatedRoute: ActivatedRoute,
                public restangular: Restangular) {}

    async ngOnInit() {
        this.activatedRoute.params.subscribe(async (data) => {
            this.journeys = await this.restangular.all('journeys').getList().toPromise();
            this.user = await this.restangular.one('users', data.id).get().toPromise();
            this.user_journeys = this.user.user_journeys;
            this.rows = this.user_journeys;
            console.log(this.rows);
            
            for (let journey of this.user_journeys){
                journey.texts = 0;
                journey.videos = 0;
                journey.images = 0;
                journey.audios = 0;
                for (let touchpoint of journey.touchpoints){
                    for (let item of touchpoint.content){
                        switch (item.type){
                            case 'audio':
                                journey.audios += 1;
                                break;
                            case 'video':
                                journey.videos += 1;
                                break;
                            case 'text':
                                journey.texts += 1;
                                break;
                            case 'image':
                                journey.images += 1;
                                break;
                            default:
                                break;
                        }
                    }
                }
    
                console.log("Splice : " , journey.videos , journey.audios , journey.images , journey.texts)
                
                if(journey.audios == 0 && journey.videos == 0 &&  journey.texts == 0 && journey.images ==0)
                {
                    this.spliceArray.push(this.i);
                }
                
                this.i++;
            }
    
            for (let sp of this.spliceArray)
            {
                this.user_journeys.splice(sp , 1);
            }
        });
    }
}