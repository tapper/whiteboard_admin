import {Routes} from '@angular/router';
import {IndexComponent} from './index/index.component';
import {ShowComponent} from './show/show.component';

export const UserJourneysRoutes: Routes = [{
    path: '',
    children: [
        {
            path: '',
            component: IndexComponent,
            data: {heading: 'User Journeys'},
        },
        {
            path: ':user_journey_id',
            component: ShowComponent,
            data: {heading: 'User Journey'},
        }
    ]
}];
