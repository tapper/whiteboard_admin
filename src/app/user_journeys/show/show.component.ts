import {ChangeDetectionStrategy, Component, OnInit, TemplateRef, ViewChild} from '@angular/core';
import {FormBuilder} from '@angular/forms';
import {Router,ActivatedRoute} from '@angular/router';
import {Restangular} from 'ngx-restangular';
import * as moment from 'moment';
import 'moment-duration-format';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';

@Component({
    selector: 'app-show',
    changeDetection: ChangeDetectionStrategy.Default,
    templateUrl: './show.component.html',
    styleUrls: ['./show.component.css'],
})
export class ShowComponent implements OnInit {

    periods: any;
    rows: any;
    selectedImage;
    imageModal: any;
    user_journey_id: number;
    user_id: number;
    sent: boolean = false;
    sortDirection: string = 'asc';

    constructor(public fb: FormBuilder,
                public activatedRoute: ActivatedRoute,
                public modalService: NgbModal,
                public restangular: Restangular,
                private router: Router) {}

    async ngOnInit() {

        this.activatedRoute.params.subscribe(async (data) => {
            this.user_id = data.id;
            this.user_journey_id = data.user_journey_id;
           this.periods = await this.restangular.one('user_journeys', data.user_journey_id).get().toPromise();
           for (let period of this.periods){
               period.formattedDuration = moment.duration(period.duration, 'seconds').format();
               period.title = period.touchpoint ? period.touchpoint.title : 'Touchpoints list';
               if (period.content && period.content.length){
                   period.audios = [];
                   period.videos = [];
                   period.images = [];
                   for (let item of period.content){
                       switch (item.type){
                           case 'audio':
                               period.audios.push(item);
                               break;
                           case 'video':
                               period.videos.push(item);
                               break;
                           case 'image':
                               period.images.push(item);
                               break;
                           default:
                               break;
                       }
                   }
               }
           }
           this.rows = this.periods;
        });
    }

    goBack() {
        this.router.navigate(['users/'+this.user_id+'/journeys']);
    }

    openImageModal (content, image) {
        this.selectedImage = image;
        this.imageModal = this.modalService.open(content);
    }

    updateFilter (event) {
        const val = event.target.value.toLowerCase();
        const temp = this.periods.filter(function(d) {
            return d.title && d.title.toLowerCase().indexOf(val) !== -1;
        });
        this.rows = temp;
    }

    async getExcel () {
        this.sent = false;
        try {
            let payload = {user_id: this.user_id, admin_id: window.localStorage.getItem('id')};
            await this.restangular.one('user_journeys', this.user_journey_id).all('excel').customPOST(payload).toPromise();
            this.sent = true;
        } catch (err){
            console.log(err);
        }
    }

    sortBy (parameter) {

        this.sortDirection = this.sortDirection === 'asc' ? 'desc' : 'asc';

        switch (parameter) {

            case 'started_at':
            case 'finished_at':
            case 'duration':
            case 'title':
                this.rows = this.periods.sort((a, b) => {
                    if (this.sortDirection === 'asc'){
                        if (a[parameter] > b[parameter]){
                            return 1;
                        }
                        if (a[parameter] < b[parameter]){
                            return -1;
                        }
                        return 0;
                    } else {
                        if (a[parameter] > b[parameter]){
                            return -1;
                        }
                        if (a[parameter] < b[parameter]){
                            return 1;
                        }
                        return 0;
                    }
                });
                break;

            case 'address':
                this.rows = this.periods.sort((a, b) => {
                    if (this.sortDirection === 'asc'){
                        if (a.address && !b.address){
                            return 1;
                        }
                        if (!a.address && b.address){
                            return -1;
                        }
                        if (a.address && b.address){
                            if (a.address > b.address){
                                return 1;
                            }
                            if (a.address < b.address){
                                return -1;
                            }
                        }
                        return 0;
                    } else {
                        if (a.address && !b.address){
                            return -1;
                        }
                        if (!a.address && b.address){
                            return 1;
                        }
                        if (a.address && b.address){
                            if (a.address > b.address){
                                return -1;
                            }
                            if (a.address < b.address){
                                return 1;
                            }
                        }
                        return 0;
                    }
                });
                break;

            default:
                break;

        }

    }

}