import {BrowserModule} from '@angular/platform-browser';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {RouterModule} from '@angular/router';
import {NgModule} from '@angular/core';
import {FormsModule} from '@angular/forms';
import {HttpClient, HttpClientModule} from '@angular/common/http';

import {TranslateLoader, TranslateModule} from '@ngx-translate/core';
import {TranslateHttpLoader} from '@ngx-translate/http-loader';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {SidebarModule} from 'ng-sidebar';

import {AppRoutes} from './app.routing';
import {AppComponent} from './app.component';
import {AdminLayoutComponent} from './layouts/admin/admin-layout.component';
import {AuthLayoutComponent} from './layouts/auth/auth-layout.component';
import {SharedModule} from './shared/shared.module';
import {AgmCoreModule} from '@agm/core';
import {google} from 'google-maps';
import {LoadingModule} from 'ngx-loading';
import {AuthGuard} from './_guards/auth.guard';
import {HttpModule} from '@angular/http';
import {ServicesModule} from './_services/services.module';
import {ApiService} from './_services/api/api.service';
import {AngularMultiSelectModule} from 'angular2-multiselect-dropdown';
import {RestangularModule} from 'ngx-restangular';
import {environment} from '../environments/environment';
import {SortablejsModule} from 'angular-sortablejs';
import {groupByPipe} from "./groupby.pipe";

import {VgCoreModule} from 'videogular2/core';
import {VgControlsModule} from 'videogular2/controls';
import {VgOverlayPlayModule} from 'videogular2/overlay-play';
import {VgBufferingModule} from 'videogular2/buffering';
import {SinglePlayerModule } from './single-player/single-player.module';


declare const google : google;

export function createTranslateLoader(http: HttpClient) {
    return new TranslateHttpLoader(http, './assets/i18n/', '.json');
}

export function RestangularConfigFactory(RestangularProvider) {

    // Setting up the Restangular endpoint - where to send queries
    RestangularProvider.setBaseUrl(environment.apiEndpoint);

    // Every time the query is made, it's console.logged.
    // Restangular doesn't support boolean answers from the API, so right now null is returned
    RestangularProvider.addResponseInterceptor((data, operation, what, url, response) => {

        if (data) {
            console.log(url, data.data, operation);
        }

        return data.data;

    });

    // Every time when error is received from the server API_ENDPOINT, first it is processed here.
    // IDM_ENDPOINT errors are processed in the functions only (login, refresh-tokens, get-users x 2).
    RestangularProvider.addErrorInterceptor(async (response, subject, responseHandler) => {

        console.log('ErrorInterceptor', response);

    });

}

@NgModule({
    declarations: [
        AppComponent,
        AdminLayoutComponent,
        AuthLayoutComponent,
        
    ],
    imports: [
        BrowserModule,
        BrowserAnimationsModule,
        SharedModule,
        RouterModule.forRoot(AppRoutes),
        FormsModule,
        HttpModule,
        HttpClientModule,
        LoadingModule,
        TranslateModule.forRoot({
            loader: {
                provide: TranslateLoader,
                useFactory: (createTranslateLoader),
                deps: [HttpClient]
            }
        }),
        NgbModule.forRoot(),
        SidebarModule.forRoot(),
        AgmCoreModule.forRoot({apiKey: "AIzaSyA4uXPidpv7gDsUIXwS30CMQNs5M-t6DOs", libraries: ["places"]}),
        ServicesModule,
        AngularMultiSelectModule,
        RestangularModule.forRoot(RestangularConfigFactory),
        SortablejsModule.forRoot({ animation: 150 }),
    ],
    providers: [ApiService, AuthGuard],
    bootstrap: [AppComponent]
})
export class AppModule {
}
