webpackJsonp(["points.module"],{

/***/ "../../../../../src/app/points/index/index.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, ".point-color {\r\n    padding: 5px 10px;\r\n    text-transform: capitalize;\r\n    width: 100px;\r\n    text-align: center;\r\n    border: 1px solid black;\r\n}\r\n\r\n.warning-color {\r\n    background-color: pink;\r\n}", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/points/index/index.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"row\">\r\n    <div class=\"col-md-10\">\r\n        <input type=\"text\" class=\"form-control mb-3\" placeholder=\"Type to filter the title column...\" required (keyup)='updateFilter($event)'>\r\n    </div>\r\n    <div class=\"col-md-2\">\r\n        <button type=\"button\" class=\"btn btn-info\" (click)=\"openCreateModal(addItem)\">New Point</button>\r\n    </div>\r\n</div>\r\n\r\n<ngx-datatable\r\n        [headerHeight]=\"40\"\r\n        [footerHeight]=\"'falsey'\"\r\n        [rowHeight]=\"'auto'\"\r\n        [scrollbarH]=\"true\"\r\n        [columnMode]=\"'force'\"\r\n        [rows]=\"rows\">\r\n\r\n    <!-- Column Templates -->\r\n\r\n    <ngx-datatable-column name=\"English title\">\r\n        <ng-template let-row=\"row\" ngx-datatable-cell-template>\r\n            <strong *ngIf=\"!editMode || editRow.id !== row.id\">{{row.title_en}}</strong>\r\n            <input *ngIf=\"editMode && editRow.id === row.id\" type=\"text\" [(ngModel)]=\"fieldsToEdit.title_en\">\r\n        </ng-template>\r\n    </ngx-datatable-column>\r\n\r\n    <ngx-datatable-column name=\"Hebrew title\">\r\n        <ng-template let-row=\"row\" ngx-datatable-cell-template>\r\n            <strong *ngIf=\"!editMode || editRow.id !== row.id\">{{row.title_he}}</strong>\r\n            <input *ngIf=\"editMode && editRow.id === row.id\" type=\"text\" [(ngModel)]=\"fieldsToEdit.title_he\">\r\n        </ng-template>\r\n    </ngx-datatable-column>\r\n\r\n    <ngx-datatable-column name=\"Color\">\r\n        <ng-template let-row=\"row\" ngx-datatable-cell-template>\r\n            <div class=\"point-color\" [style.backgroundColor]=\"row.color\" *ngIf=\"!editMode || editRow.id !== row.id\">{{row.color}}</div>\r\n            <input *ngIf=\"editMode && editRow.id === row.id\" type=\"color\" [(ngModel)]=\"fieldsToEdit.color\">\r\n        </ng-template>\r\n    </ngx-datatable-column>\r\n\r\n    <ngx-datatable-column name=\"Options\" [sortable]=\"false\" [maxWidth]=\"200\">\r\n        <ng-template let-row=\"row\" ngx-datatable-cell-template>\r\n            <button type=\"button\" *ngIf=\"!editMode || editRow.id !== row.id\" [disabled]=\"editMode\" class=\"btn btn-warning\" (click)=\"switchToEditMode(row)\">Edit</button>\r\n            <button type=\"button\" *ngIf=\"!editMode || editRow.id !== row.id\" [disabled]=\"editMode\" class=\"btn btn-danger\" (click)=\"openDeleteModal(content, row)\">Delete</button>\r\n            <button type=\"button\" *ngIf=\"editMode && editRow.id === row.id\" class=\"btn btn-info\" (click)=\"editItem(row)\">Save</button>\r\n            <button type=\"button\" *ngIf=\"editMode && editRow.id === row.id\" class=\"btn btn-danger\" (click)=\"cancelEdit()\">Cancel</button>\r\n        </ng-template>\r\n    </ngx-datatable-column>\r\n\r\n</ngx-datatable>\r\n\r\n<ng-template ngbModalContainer></ng-template>\r\n<ng-template #content let-c=\"close\" let-d=\"dismiss\">\r\n    <div class=\"modal-header\">\r\n        <h6 class=\"modal-title text-uppercase\">Delete point</h6>\r\n        <button type=\"button\" class=\"close\" aria-label=\"Close\" (click)=\"d()\">\r\n            <span aria-hidden=\"true\">&times;</span>\r\n        </button>\r\n    </div>\r\n    <div class=\"modal-body\">Are you sure that you want to delete this point? It will delete all content that is connected to this point in all journeys.</div>\r\n    <div class=\"modal-footer\">\r\n        <button type=\"button\" class=\"btn btn-secondary\" (click)=\"c()\">Close</button>\r\n        <button type=\"button\" class=\"btn btn-primary\" (click)=\"deleteItem()\">Delete</button>\r\n    </div>\r\n</ng-template>\r\n\r\n<ng-template ngbModalContainer></ng-template>\r\n<ng-template #addItem let-c=\"close\" let-d=\"dismiss\">\r\n    <div class=\"modal-header\">\r\n        <h6 class=\"modal-title text-uppercase\">New point</h6>\r\n        <button type=\"button\" class=\"close\" aria-label=\"סגור\" (click)=\"d()\">\r\n            <span aria-hidden=\"true\">&times;</span>\r\n        </button>\r\n    </div>\r\n    <div class=\"modal-body\">\r\n        <form class=\"form-horizontal\" novalidate [formGroup]=\"form\">\r\n            <div class=\"row\">\r\n                <div class=\"col-5\">\r\n                    <div class=\"form-group\">\r\n                        <input class=\"form-control\" type=\"text\" formControlName=\"title_en\" placeholder=\"English title\">\r\n                    </div>\r\n                </div>\r\n                <div class=\"col-5\">\r\n                    <div class=\"form-group\">\r\n                        <input class=\"form-control\" type=\"text\" formControlName=\"title_he\" placeholder=\"Hebrew title\">\r\n                    </div>\r\n                </div>\r\n                <div class=\"col-2\">\r\n                    <div class=\"form-group\">\r\n                        <input type=\"color\" formControlName=\"color\">\r\n                    </div>\r\n                </div>\r\n            </div>\r\n        </form>\r\n    </div>\r\n    <div class=\"modal-footer\">\r\n        <button type=\"button\" class=\"btn btn-secondary\" (click)=\"c()\">Close</button>\r\n        <button type=\"button\" class=\"btn btn-info\" type=\"submit\" [disabled]=\"form.invalid\" (click)=\"createItem()\">Save</button>\r\n    </div>\r\n</ng-template>"

/***/ }),

/***/ "../../../../../src/app/points/index/index.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return IndexComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ngx_restangular__ = __webpack_require__("../../../../ngx-restangular/dist/esm/src/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ngx_restangular___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_ngx_restangular__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ng_bootstrap_ng_bootstrap__ = __webpack_require__("../../../../@ng-bootstrap/ng-bootstrap/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_forms__ = __webpack_require__("../../../forms/@angular/forms.es5.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = y[op[0] & 2 ? "return" : op[0] ? "throw" : "next"]) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [0, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};




var IndexComponent = (function () {
    function IndexComponent(restangular, modalService, fb) {
        this.restangular = restangular;
        this.modalService = modalService;
        this.fb = fb;
        this.editMode = false;
        this.editRow = null;
        this.fieldsToEdit = { title_en: '', title_he: '', color: '' };
        this.form = this.fb.group({
            title_en: new __WEBPACK_IMPORTED_MODULE_3__angular_forms__["FormControl"]('', __WEBPACK_IMPORTED_MODULE_3__angular_forms__["Validators"].required),
            title_he: new __WEBPACK_IMPORTED_MODULE_3__angular_forms__["FormControl"]('', __WEBPACK_IMPORTED_MODULE_3__angular_forms__["Validators"].required),
            color: new __WEBPACK_IMPORTED_MODULE_3__angular_forms__["FormControl"]('#FFFFFF', __WEBPACK_IMPORTED_MODULE_3__angular_forms__["Validators"].required),
        });
    }
    IndexComponent.prototype.ngOnInit = function () {
        return __awaiter(this, void 0, void 0, function () {
            var _a;
            return __generator(this, function (_b) {
                switch (_b.label) {
                    case 0:
                        _a = this;
                        return [4 /*yield*/, this.restangular.all('points').getList().toPromise()];
                    case 1:
                        _a.rows = _b.sent();
                        this.points = this.rows;
                        return [2 /*return*/];
                }
            });
        });
    };
    IndexComponent.prototype.updateFilter = function (event) {
        var val = event.target.value.toLowerCase();
        var temp = this.points.filter(function (d) {
            return d.title_en && d.title_en.toLowerCase().indexOf(val) !== -1 || d.title_he && d.title_he.toLowerCase().indexOf(val) !== -1;
        });
        this.rows = temp;
    };
    IndexComponent.prototype.switchToEditMode = function (item) {
        this.editMode = true;
        this.editRow = item;
        this.fieldsToEdit.title_en = item.title_en;
        this.fieldsToEdit.title_he = item.title_he;
        this.fieldsToEdit.color = item.color;
    };
    IndexComponent.prototype.editItem = function () {
        return __awaiter(this, void 0, void 0, function () {
            var _a;
            return __generator(this, function (_b) {
                switch (_b.label) {
                    case 0:
                        this.editRow.title_en = this.fieldsToEdit.title_en;
                        this.editRow.title_he = this.fieldsToEdit.title_he;
                        this.editRow.color = this.fieldsToEdit.color;
                        return [4 /*yield*/, this.editRow.patch().toPromise()];
                    case 1:
                        _b.sent();
                        _a = this;
                        return [4 /*yield*/, this.restangular.all('points').getList().toPromise()];
                    case 2:
                        _a.rows = _b.sent();
                        this.points = this.rows;
                        this.cancelEdit();
                        return [2 /*return*/];
                }
            });
        });
    };
    IndexComponent.prototype.cancelEdit = function () {
        this.editMode = false;
        this.editRow = null;
    };
    IndexComponent.prototype.deleteItem = function () {
        return __awaiter(this, void 0, void 0, function () {
            var _a;
            return __generator(this, function (_b) {
                switch (_b.label) {
                    case 0: return [4 /*yield*/, this.itemToDelete.remove().toPromise()];
                    case 1:
                        _b.sent();
                        this.deleteModal.close();
                        _a = this;
                        return [4 /*yield*/, this.restangular.all('points').getList().toPromise()];
                    case 2:
                        _a.rows = _b.sent();
                        this.points = this.rows;
                        return [2 /*return*/];
                }
            });
        });
    };
    IndexComponent.prototype.openDeleteModal = function (content, region) {
        this.deleteModal = this.modalService.open(content);
        this.itemToDelete = region;
    };
    IndexComponent.prototype.openCreateModal = function (content) {
        this.createModal = this.modalService.open(content);
    };
    IndexComponent.prototype.createItem = function () {
        return __awaiter(this, void 0, void 0, function () {
            var point, _a;
            return __generator(this, function (_b) {
                switch (_b.label) {
                    case 0:
                        point = this.restangular.restangularizeElement('', { title_en: this.form.value.title_en, title_he: this.form.value.title_he, color: this.form.value.color }, 'points');
                        return [4 /*yield*/, point.save().toPromise()];
                    case 1:
                        _b.sent();
                        this.createModal.close();
                        _a = this;
                        return [4 /*yield*/, this.restangular.all('points').getList().toPromise()];
                    case 2:
                        _a.rows = _b.sent();
                        this.points = this.rows;
                        return [2 /*return*/];
                }
            });
        });
    };
    IndexComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-points',
            template: __webpack_require__("../../../../../src/app/points/index/index.component.html"),
            styles: [__webpack_require__("../../../../../src/app/points/index/index.component.css")]
        }),
        __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1_ngx_restangular__["Restangular"] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1_ngx_restangular__["Restangular"]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_2__ng_bootstrap_ng_bootstrap__["a" /* NgbModal */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__ng_bootstrap_ng_bootstrap__["a" /* NgbModal */]) === "function" && _b || Object, typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_3__angular_forms__["FormBuilder"] !== "undefined" && __WEBPACK_IMPORTED_MODULE_3__angular_forms__["FormBuilder"]) === "function" && _c || Object])
    ], IndexComponent);
    return IndexComponent;
    var _a, _b, _c;
}());

//# sourceMappingURL=index.component.js.map

/***/ }),

/***/ "../../../../../src/app/points/points.module.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PointsModule", function() { return PointsModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_common__ = __webpack_require__("../../../common/@angular/common.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__("../../../router/@angular/router.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__swimlane_ngx_datatable__ = __webpack_require__("../../../../@swimlane/ngx-datatable/release/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__swimlane_ngx_datatable___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3__swimlane_ngx_datatable__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__index_index_component__ = __webpack_require__("../../../../../src/app/points/index/index.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_ng2_file_upload_ng2_file_upload__ = __webpack_require__("../../../../ng2-file-upload/ng2-file-upload.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_ng2_file_upload_ng2_file_upload___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_5_ng2_file_upload_ng2_file_upload__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_angular_tree_component__ = __webpack_require__("../../../../angular-tree-component/dist/angular-tree-component.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7_ng2_validation__ = __webpack_require__("../../../../ng2-validation/dist/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7_ng2_validation___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_7_ng2_validation__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__angular_forms__ = __webpack_require__("../../../forms/@angular/forms.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__ng_bootstrap_ng_bootstrap__ = __webpack_require__("../../../../@ng-bootstrap/ng-bootstrap/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10_angular2_text_mask__ = __webpack_require__("../../../../angular2-text-mask/dist/angular2TextMask.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10_angular2_text_mask___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_10_angular2_text_mask__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11_ngx_loading__ = __webpack_require__("../../../../ngx-loading/ngx-loading/ngx-loading.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__points_routing__ = __webpack_require__("../../../../../src/app/points/points.routing.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};













var PointsModule = (function () {
    function PointsModule() {
    }
    PointsModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_2__angular_core__["NgModule"])({
            imports: [__WEBPACK_IMPORTED_MODULE_0__angular_common__["CommonModule"],
                __WEBPACK_IMPORTED_MODULE_1__angular_router__["d" /* RouterModule */].forChild(__WEBPACK_IMPORTED_MODULE_12__points_routing__["a" /* PointsRoutes */]),
                __WEBPACK_IMPORTED_MODULE_3__swimlane_ngx_datatable__["NgxDatatableModule"],
                __WEBPACK_IMPORTED_MODULE_8__angular_forms__["FormsModule"],
                __WEBPACK_IMPORTED_MODULE_8__angular_forms__["ReactiveFormsModule"],
                __WEBPACK_IMPORTED_MODULE_9__ng_bootstrap_ng_bootstrap__["c" /* NgbProgressbarModule */],
                __WEBPACK_IMPORTED_MODULE_7_ng2_validation__["CustomFormsModule"],
                __WEBPACK_IMPORTED_MODULE_6_angular_tree_component__["a" /* TreeModule */],
                __WEBPACK_IMPORTED_MODULE_5_ng2_file_upload_ng2_file_upload__["FileUploadModule"],
                __WEBPACK_IMPORTED_MODULE_11_ngx_loading__["a" /* LoadingModule */],
                __WEBPACK_IMPORTED_MODULE_10_angular2_text_mask__["TextMaskModule"]],
            declarations: [__WEBPACK_IMPORTED_MODULE_4__index_index_component__["a" /* IndexComponent */]]
        })
    ], PointsModule);
    return PointsModule;
}());

//# sourceMappingURL=points.module.js.map

/***/ }),

/***/ "../../../../../src/app/points/points.routing.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return PointsRoutes; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__index_index_component__ = __webpack_require__("../../../../../src/app/points/index/index.component.ts");

var PointsRoutes = [{
        path: '',
        children: [{
                path: '',
                component: __WEBPACK_IMPORTED_MODULE_0__index_index_component__["a" /* IndexComponent */],
                data: { heading: 'Points' },
            }
        ]
    }];
//# sourceMappingURL=points.routing.js.map

/***/ })

});
//# sourceMappingURL=points.module.chunk.js.map