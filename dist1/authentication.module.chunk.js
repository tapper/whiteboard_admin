webpackJsonp(["authentication.module"],{

/***/ "../../../../../src/app/authentication/authentication.module.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AuthenticationModule", function() { return AuthenticationModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__("../../../router/@angular/router.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_common__ = __webpack_require__("../../../common/@angular/common.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_forms__ = __webpack_require__("../../../forms/@angular/forms.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__authentication_routing__ = __webpack_require__("../../../../../src/app/authentication/authentication.routing.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__signin_signin_component__ = __webpack_require__("../../../../../src/app/authentication/signin/signin.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__signup_signup_component__ = __webpack_require__("../../../../../src/app/authentication/signup/signup.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__forgot_forgot_component__ = __webpack_require__("../../../../../src/app/authentication/forgot/forgot.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__lockscreen_lockscreen_component__ = __webpack_require__("../../../../../src/app/authentication/lockscreen/lockscreen.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__services_services_module__ = __webpack_require__("../../../../../src/app/_services/services.module.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};










var AuthenticationModule = (function () {
    function AuthenticationModule() {
    }
    AuthenticationModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            imports: [
                __WEBPACK_IMPORTED_MODULE_2__angular_common__["CommonModule"],
                __WEBPACK_IMPORTED_MODULE_1__angular_router__["d" /* RouterModule */].forChild(__WEBPACK_IMPORTED_MODULE_4__authentication_routing__["a" /* AuthenticationRoutes */]),
                __WEBPACK_IMPORTED_MODULE_3__angular_forms__["FormsModule"],
                __WEBPACK_IMPORTED_MODULE_3__angular_forms__["ReactiveFormsModule"],
                __WEBPACK_IMPORTED_MODULE_9__services_services_module__["a" /* ServicesModule */]
            ],
            declarations: [__WEBPACK_IMPORTED_MODULE_5__signin_signin_component__["a" /* SigninComponent */], __WEBPACK_IMPORTED_MODULE_6__signup_signup_component__["a" /* SignupComponent */], __WEBPACK_IMPORTED_MODULE_7__forgot_forgot_component__["a" /* ForgotComponent */], __WEBPACK_IMPORTED_MODULE_8__lockscreen_lockscreen_component__["a" /* LockscreenComponent */]]
        })
    ], AuthenticationModule);
    return AuthenticationModule;
}());

//# sourceMappingURL=authentication.module.js.map

/***/ }),

/***/ "../../../../../src/app/authentication/authentication.routing.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AuthenticationRoutes; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__signin_signin_component__ = __webpack_require__("../../../../../src/app/authentication/signin/signin.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__signup_signup_component__ = __webpack_require__("../../../../../src/app/authentication/signup/signup.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__forgot_forgot_component__ = __webpack_require__("../../../../../src/app/authentication/forgot/forgot.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__lockscreen_lockscreen_component__ = __webpack_require__("../../../../../src/app/authentication/lockscreen/lockscreen.component.ts");




var AuthenticationRoutes = [
    {
        path: '',
        children: [{
                path: '',
                component: __WEBPACK_IMPORTED_MODULE_0__signin_signin_component__["a" /* SigninComponent */]
            }, {
                path: 'signup',
                component: __WEBPACK_IMPORTED_MODULE_1__signup_signup_component__["a" /* SignupComponent */]
            }, {
                path: 'forgot',
                component: __WEBPACK_IMPORTED_MODULE_2__forgot_forgot_component__["a" /* ForgotComponent */]
            }, {
                path: 'lockscreen',
                component: __WEBPACK_IMPORTED_MODULE_3__lockscreen_lockscreen_component__["a" /* LockscreenComponent */]
            }]
    }
];
//# sourceMappingURL=authentication.routing.js.map

/***/ }),

/***/ "../../../../../src/app/authentication/forgot/forgot.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"d-flex align-items-stretch min-height-100 h-100\">\r\n  <div class=\"bg-cover bg-3 d-none d-md-inline-flex col-md-6 col-lg-8\"></div>\r\n  <div class=\"card card-body mb-0 rounded-0 p-5 col-sm-12 col-md-6 col-lg-4\">\r\n    <form [formGroup]=\"form\" (ngSubmit)=\"onSubmit()\">\r\n      <div class=\"mb-3\">\r\n        <img src=\"assets/images/logo.png\" class=\"avatar-xs mb-1\" alt=\"\">\r\n        <p class=\"ff-headers text-uppercase\">Recover your password</p>\r\n      </div>\r\n      <fieldset class=\"form-group\" [ngClass]=\"{'has-danger': form.controls['uname'].hasError('required') && form.controls['uname'].touched}\">\r\n        <label for=\"username\" class=\"form-control-label\">\r\n          Enter your username\r\n        </label>\r\n        <input type=\"text\" class=\"form-control\" placeholder=\"Username\" [formControl]=\"form.controls['uname']\" [ngClass]=\"{'form-control-danger': form.controls['uname'].hasError('required') && form.controls['uname'].touched}\"/>\r\n        <div class=\"form-control-feedback\" *ngIf=\"form.controls['uname'].hasError('required') && form.controls['uname'].touched\">Username is required.</div>\r\n      </fieldset>\r\n      <button class=\"btn btn-primary\" type=\"submit\" [disabled]=\"!form.valid\">Reset password</button>\r\n    </form>\r\n  </div>\r\n</div>"

/***/ }),

/***/ "../../../../../src/app/authentication/forgot/forgot.component.scss":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/authentication/forgot/forgot.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ForgotComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__("../../../router/@angular/router.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_forms__ = __webpack_require__("../../../forms/@angular/forms.es5.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var ForgotComponent = (function () {
    function ForgotComponent(fb, router) {
        this.fb = fb;
        this.router = router;
    }
    ForgotComponent.prototype.ngOnInit = function () {
        this.form = this.fb.group({
            uname: [null, __WEBPACK_IMPORTED_MODULE_2__angular_forms__["Validators"].compose([__WEBPACK_IMPORTED_MODULE_2__angular_forms__["Validators"].required])]
        });
    };
    ForgotComponent.prototype.onSubmit = function () {
        this.router.navigate(['/']);
    };
    ForgotComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-forgot',
            template: __webpack_require__("../../../../../src/app/authentication/forgot/forgot.component.html"),
            styles: [__webpack_require__("../../../../../src/app/authentication/forgot/forgot.component.scss")]
        }),
        __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_2__angular_forms__["FormBuilder"] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__angular_forms__["FormBuilder"]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_1__angular_router__["c" /* Router */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__angular_router__["c" /* Router */]) === "function" && _b || Object])
    ], ForgotComponent);
    return ForgotComponent;
    var _a, _b;
}());

//# sourceMappingURL=forgot.component.js.map

/***/ }),

/***/ "../../../../../src/app/authentication/lockscreen/lockscreen.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"d-flex align-items-center justify-content-md-center min-height-100 text-center bg-indigo-A700 h-100\">\r\n  <div class=\"col col-lg-10 p-5\">\r\n    <img src=\"assets/images/avatar.jpg\" class=\"avatar-md rounded-circle\" alt=\"user\" title=\"user\"/>\r\n    <div class=\"mb-3 mt-3\">\r\n      <p class=\"ff-headers text-uppercase mb-0\">Betty Simmons</p>\r\n      <small>Please enter your lock code</small>\r\n    </div>\r\n    <div class=\"center-block lockcode mt-1\">\r\n      <form role=\"form\" (ngSubmit)=\"onSubmit()\">\r\n        <input type=\"text\" class=\"form-control\" placeholder=\"Enter passcode and press enter\">\r\n      </form>\r\n    </div>\r\n  </div>\r\n</div>\r\n"

/***/ }),

/***/ "../../../../../src/app/authentication/lockscreen/lockscreen.component.scss":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/authentication/lockscreen/lockscreen.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return LockscreenComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__("../../../router/@angular/router.es5.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var LockscreenComponent = (function () {
    function LockscreenComponent(router) {
        this.router = router;
    }
    LockscreenComponent.prototype.onSubmit = function () {
        this.router.navigate(['/']);
    };
    LockscreenComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-lockscreen',
            template: __webpack_require__("../../../../../src/app/authentication/lockscreen/lockscreen.component.html"),
            styles: [__webpack_require__("../../../../../src/app/authentication/lockscreen/lockscreen.component.scss")]
        }),
        __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__angular_router__["c" /* Router */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__angular_router__["c" /* Router */]) === "function" && _a || Object])
    ], LockscreenComponent);
    return LockscreenComponent;
    var _a;
}());

//# sourceMappingURL=lockscreen.component.js.map

/***/ }),

/***/ "../../../../../src/app/authentication/signin/signin.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"d-flex align-items-stretch min-height-100 h-100\">\r\n  <div class=\"bg-cover bg-1 d-none d-md-inline-flex col-md-6 col-lg-8\"></div>\r\n  <div class=\"card card-body mb-0 rounded-0 p-5 col-sm-12 col-md-6 col-lg-4\">\r\n    <form [formGroup]=\"form\" (ngSubmit)=\"onSubmit()\">\r\n      <div class=\"mb-3\">\r\n        <img src=\"assets/images/logo.png\" class=\"avatar-xs mb-1\" alt=\"\">\r\n        <p class=\"ff-headers text-uppercase\">Sign in to your account</p>\r\n      </div>\r\n      <fieldset class=\"form-group\" [ngClass]=\"{'has-danger': form.controls['uname'].hasError('required') && form.controls['uname'].touched}\">\r\n        <label for=\"username\" class=\"form-control-label\">\r\n          Enter your username\r\n        </label>\r\n        <input type=\"text\" class=\"form-control\" placeholder=\"username\" [formControl]=\"form.controls['uname']\" [ngClass]=\"{'form-control-danger': form.controls['uname'].hasError('required') && form.controls['uname'].touched}\"/>\r\n        <div class=\"form-control-feedback\" *ngIf=\"form.controls['uname'].hasError('required') && form.controls['uname'].touched\">Username is required.</div>\r\n      </fieldset>\r\n      <fieldset class=\"form-group\" [ngClass]=\"{'has-danger': form.controls['password'].hasError('required') && form.controls['password'].touched}\">\r\n        <label for=\"password\" class=\"form-control-label\">\r\n          Enter your password\r\n        </label>\r\n        <input type=\"password\" class=\"form-control\" placeholder=\"********\" [formControl]=\"form.controls['password']\" [ngClass]=\"{'form-control-danger': form.controls['password'].hasError('required') && form.controls['password'].touched}\"/>\r\n        <div class=\"form-control-feedback\" *ngIf=\"form.controls['password'].hasError('required') && form.controls['password'].touched\">Password is required.</div>\r\n        <!--<a [routerLink]=\"['/authentication/forgot']\" class=\"form-text\"><small>Recover password</small></a>-->\r\n      </fieldset>\r\n      <!--<label class=\"custom-control custom-checkbox mb-3\">-->\r\n        <!--<input type=\"checkbox\" class=\"custom-control-input\">-->\r\n        <!--<span class=\"custom-control-indicator\"></span>-->\r\n        <!--<span class=\"custom-control-description\">Stay logged in</span>-->\r\n      <!--</label>-->\r\n      <div class=\"block\"></div>\r\n      <button class=\"btn btn-primary\" type=\"submit\" [disabled]=\"!form.valid\">Login</button>\r\n    </form>\r\n    <div class=\"alert alert-danger mt-3\" role=\"alert\" *ngIf=\"wrongCredentials\">\r\n      There is no user with this username and password.\r\n    </div>\r\n  </div>\r\n</div>"

/***/ }),

/***/ "../../../../../src/app/authentication/signin/signin.component.scss":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/authentication/signin/signin.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SigninComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__("../../../router/@angular/router.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_forms__ = __webpack_require__("../../../forms/@angular/forms.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_ngx_restangular__ = __webpack_require__("../../../../ngx-restangular/dist/esm/src/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_ngx_restangular___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_ngx_restangular__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = y[op[0] & 2 ? "return" : op[0] ? "throw" : "next"]) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [0, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};




var SigninComponent = (function () {
    function SigninComponent(fb, router, restangular) {
        this.fb = fb;
        this.router = router;
        this.restangular = restangular;
        this.wrongCredentials = false;
    }
    SigninComponent.prototype.ngOnInit = function () {
        this.form = this.fb.group({
            uname: [null, __WEBPACK_IMPORTED_MODULE_2__angular_forms__["Validators"].compose([__WEBPACK_IMPORTED_MODULE_2__angular_forms__["Validators"].required])],
            password: [null, __WEBPACK_IMPORTED_MODULE_2__angular_forms__["Validators"].compose([__WEBPACK_IMPORTED_MODULE_2__angular_forms__["Validators"].required])]
        });
    };
    SigninComponent.prototype.onSubmit = function () {
        return __awaiter(this, void 0, void 0, function () {
            var response, err_1;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        this.wrongCredentials = false;
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        return [4 /*yield*/, this.restangular.all('tokens')
                                .customPOST({ username: this.form.value.uname, password: this.form.value.password })
                                .toPromise()];
                    case 2:
                        response = _a.sent();
                        localStorage.setItem('id', response.id);
                        this.router.navigate(['/']);
                        return [3 /*break*/, 4];
                    case 3:
                        err_1 = _a.sent();
                        console.log(err_1);
                        this.wrongCredentials = true;
                        return [3 /*break*/, 4];
                    case 4: return [2 /*return*/];
                }
            });
        });
    };
    SigninComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-signin',
            template: __webpack_require__("../../../../../src/app/authentication/signin/signin.component.html"),
            styles: [__webpack_require__("../../../../../src/app/authentication/signin/signin.component.scss")]
        }),
        __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_2__angular_forms__["FormBuilder"] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__angular_forms__["FormBuilder"]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_1__angular_router__["c" /* Router */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__angular_router__["c" /* Router */]) === "function" && _b || Object, typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_3_ngx_restangular__["Restangular"] !== "undefined" && __WEBPACK_IMPORTED_MODULE_3_ngx_restangular__["Restangular"]) === "function" && _c || Object])
    ], SigninComponent);
    return SigninComponent;
    var _a, _b, _c;
}());

//# sourceMappingURL=signin.component.js.map

/***/ }),

/***/ "../../../../../src/app/authentication/signup/signup.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"d-flex align-items-stretch flex-nowrap min-height-100 h-100\">\r\n  <div class=\"bg-cover bg-2 d-none d-md-inline-flex col-md-6 col-lg-8\"></div>\r\n  <div class=\"card card-body mb-0 rounded-0 p-5 col-sm-12 col-md-6 col-lg-4\">\r\n    <form [formGroup]=\"form\" (ngSubmit)=\"onSubmit()\">\r\n      <div class=\"mb-3\">\r\n        <img src=\"assets/images/logo.png\" class=\"avatar-xs mb-1\" alt=\"\">\r\n        <p class=\"ff-headers text-uppercase\">Create an account</p>\r\n      </div>\r\n      <fieldset class=\"form-group\" [ngClass]=\"{'has-danger': form.controls['uname'].hasError('required') && form.controls['uname'].touched}\">\r\n        <label for=\"username\">\r\n          Choose a username\r\n        </label>\r\n        <input type=\"text\" class=\"form-control\" placeholder=\"username\" [formControl]=\"form.controls['uname']\" [ngClass]=\"{'form-control-danger': form.controls['uname'].hasError('required') && form.controls['uname'].touched}\"/>\r\n        <div class=\"form-control-feedback\" *ngIf=\"form.controls['uname'].hasError('required') && form.controls['uname'].touched\">Username is required.</div>\r\n      </fieldset>\r\n      <fieldset class=\"form-group\" [ngClass]=\"{'has-danger': form.controls['password'].hasError('required') && form.controls['password'].touched}\">\r\n        <label for=\"password\">\r\n          Enter your password\r\n        </label>\r\n        <input type=\"password\" class=\"form-control\" placeholder=\"********\" [formControl]=\"form.controls['password']\" [ngClass]=\"{'form-control-danger': form.controls['password'].hasError('required') && form.controls['password'].touched}\"/>\r\n        <div class=\"form-control-feedback\" *ngIf=\"form.controls['password'].hasError('required') && form.controls['password'].touched\">Password is required.</div>\r\n      </fieldset>\r\n      <fieldset class=\"form-group\" [ngClass]=\"{'has-danger': form.controls['confirmPassword'].hasError('required') && form.controls['confirmPassword'].touched}\">\r\n        <label for=\"password\">\r\n          Confirm your password\r\n        </label>\r\n        <input type=\"password\" class=\"form-control\" placeholder=\"********\" [formControl]=\"form.controls['confirmPassword']\" [ngClass]=\"{'form-control-danger': form.controls['confirmPassword'].hasError('required') && form.controls['confirmPassword'].touched}\"/>\r\n        <div class=\"form-control-feedback\" *ngIf=\"form.controls['confirmPassword'].hasError('required') && form.controls['confirmPassword'].touched\">Confirm your password.</div>\r\n      </fieldset>\r\n      <label class=\"custom-control custom-checkbox mb-1\">\r\n        <input type=\"checkbox\" class=\"custom-control-input\">\r\n        <span class=\"custom-control-indicator\"></span>\r\n        <span class=\"custom-control-description\">I have read and agree to the terms of service.</span>\r\n      </label>\r\n      <div class=\"block mb-3\"></div>\r\n      <button class=\"btn btn-primary\" type=\"submit\" [disabled]=\"!form.valid\">Create</button>\r\n      <a class=\"btn btn-outline-primary btn-link\" [routerLink]=\"['/authentication/signin']\">Login!</a>\r\n    </form>\r\n  </div>\r\n</div>"

/***/ }),

/***/ "../../../../../src/app/authentication/signup/signup.component.scss":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/authentication/signup/signup.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SignupComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__("../../../router/@angular/router.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_forms__ = __webpack_require__("../../../forms/@angular/forms.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_ng2_validation__ = __webpack_require__("../../../../ng2-validation/dist/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_ng2_validation___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_ng2_validation__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var password = new __WEBPACK_IMPORTED_MODULE_2__angular_forms__["FormControl"]('', __WEBPACK_IMPORTED_MODULE_2__angular_forms__["Validators"].required);
var confirmPassword = new __WEBPACK_IMPORTED_MODULE_2__angular_forms__["FormControl"]('', __WEBPACK_IMPORTED_MODULE_3_ng2_validation__["CustomValidators"].equalTo(password));
var SignupComponent = (function () {
    function SignupComponent(fb, router) {
        this.fb = fb;
        this.router = router;
    }
    SignupComponent.prototype.ngOnInit = function () {
        this.form = this.fb.group({
            uname: [null, __WEBPACK_IMPORTED_MODULE_2__angular_forms__["Validators"].compose([__WEBPACK_IMPORTED_MODULE_2__angular_forms__["Validators"].required])],
            password: password,
            confirmPassword: confirmPassword
        });
    };
    SignupComponent.prototype.onSubmit = function () {
        this.router.navigate(['/']);
    };
    SignupComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-signup',
            template: __webpack_require__("../../../../../src/app/authentication/signup/signup.component.html"),
            styles: [__webpack_require__("../../../../../src/app/authentication/signup/signup.component.scss")]
        }),
        __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_2__angular_forms__["FormBuilder"] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__angular_forms__["FormBuilder"]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_1__angular_router__["c" /* Router */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__angular_router__["c" /* Router */]) === "function" && _b || Object])
    ], SignupComponent);
    return SignupComponent;
    var _a, _b;
}());

//# sourceMappingURL=signup.component.js.map

/***/ })

});
//# sourceMappingURL=authentication.module.chunk.js.map