webpackJsonp(["journeys.module"],{

/***/ "../../../../../src/app/journeys/create/create.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, ".wide-input {\r\n    width: 100% !important;\r\n    border: 1px solid grey;\r\n}\r\n\r\n.main-div {\r\n    padding: 5px 15px 15px 15px;\r\n    background-color: white;\r\n}\r\n\r\n.company-media {\r\n    display: -ms-flexbox;\r\n    display: flex;\r\n    -ms-flex-direction: row;\r\n        flex-direction: row;\r\n    -ms-flex-line-pack: center;\r\n        align-content: center;\r\n    -ms-flex-align: end;\r\n        align-items: flex-end;\r\n}\r\n\r\n.company-media img {\r\n    width: 30%;\r\n    border: 1px solid grey;\r\n}\r\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/journeys/create/create.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"main-div\">\r\n\r\n    <form class=\"form-horizontal\" novalidate [formGroup]=\"form\">\r\n\r\n        <div class=\"row\">\r\n\r\n            <div class=\"form-group col-md-6\">\r\n                <label class=\"col-form-label\">English title</label>\r\n                <input class=\"form-control\" formControlName=\"title_en\" type=\"text\">\r\n            </div>\r\n\r\n            <div class=\"form-group col-md-6\">\r\n                <label class=\"col-form-label\">Hebrew title</label>\r\n                <input class=\"form-control\" formControlName=\"title_he\" type=\"text\">\r\n            </div>\r\n\r\n        </div>\r\n\r\n        <div class=\"row mt-2\">\r\n            <div class=\"form-group col-md-12\">\r\n                <angular2-multiselect [data]=\"points\"\r\n                                      *ngIf=\"points\"\r\n                                      formControlName=\"points\"\r\n                                      [settings]=\"dropdownSettings\">\r\n                </angular2-multiselect>\r\n            </div>\r\n        </div>\r\n\r\n        <div class=\"row company-media\">\r\n\r\n            <div class=\"form-group col-md-6\">\r\n                <label class=\"col-form-label\">Choose picture</label>\r\n                <input class=\"wide-input\" name=\"logo\" (change)=\"onFileChange($event, 'logo')\" type=\"file\" accept=\"image/*\">\r\n            </div>\r\n\r\n            <div class=\"form-group col-md-6\">\r\n                <img [src]=\"logo.path\" *ngIf=\"logo.path\">\r\n            </div>\r\n\r\n        </div>\r\n\r\n        <!-- new -->\r\n        <div class=\"row company-media\">\r\n\r\n            <div class=\"form-group col-md-6\">\r\n                <label class=\"col-form-label\">Choose inside picture</label>\r\n                <input class=\"wide-input\" name=\"logo\" (change)=\"onFileChange($event, 'logo2')\" type=\"file\" accept=\"image/*\">\r\n            </div>\r\n\r\n            <div class=\"form-group col-md-6\">\r\n                <img [src]=\"logo2.path\" *ngIf=\"logo2.path\">\r\n            </div>\r\n\r\n        </div>\r\n        <!-- new -->\r\n\r\n        <div class=\"form-group col-md-6\" style=\"text-align: left !important;\">\r\n            <!--<button type=\"submit\" (click)=\"onSubmit()\" class=\"btn btn-primary\">Send</button>-->\r\n            <button type=\"submit\" (click)=\"onSubmit()\" class=\"btn btn-primary\" [disabled]=\"form.invalid\">Send</button>\r\n        </div>\r\n\r\n    </form>\r\n\r\n    <div class=\"row\" *ngIf=\"errors.length > 0\">\r\n        <div class=\"alert alert-warning\" role=\"alert\" *ngFor=\"let item of errors\">{{item.message}}</div>\r\n    </div>\r\n\r\n</div>\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n"

/***/ }),

/***/ "../../../../../src/app/journeys/create/create.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return CreateComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_forms__ = __webpack_require__("../../../forms/@angular/forms.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_router__ = __webpack_require__("../../../router/@angular/router.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_platform_browser__ = __webpack_require__("../../../platform-browser/@angular/platform-browser.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_ngx_restangular__ = __webpack_require__("../../../../ngx-restangular/dist/esm/src/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_ngx_restangular___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4_ngx_restangular__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = y[op[0] & 2 ? "return" : op[0] ? "throw" : "next"]) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [0, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};





var CreateComponent = (function () {
    function CreateComponent(fb, router, sanitizer, restangular) {
        this.fb = fb;
        this.router = router;
        this.sanitizer = sanitizer;
        this.restangular = restangular;
        this.form = this.fb.group({
            title_en: new __WEBPACK_IMPORTED_MODULE_1__angular_forms__["FormControl"]('', __WEBPACK_IMPORTED_MODULE_1__angular_forms__["Validators"].required),
            title_he: new __WEBPACK_IMPORTED_MODULE_1__angular_forms__["FormControl"]('', __WEBPACK_IMPORTED_MODULE_1__angular_forms__["Validators"].required),
            points: new __WEBPACK_IMPORTED_MODULE_1__angular_forms__["FormControl"]([], __WEBPACK_IMPORTED_MODULE_1__angular_forms__["Validators"].required),
        });
        this.logo = { path: null, file: null };
        this.logo2 = { path: null, file: null };
        this.errors = [];
        this.points = [];
        this.dropdownSettings = {
            singleSelection: false,
            text: "Select points",
            selectAllText: 'Select All',
            unSelectAllText: 'UnSelect All'
        };
    }
    CreateComponent.prototype.ngOnInit = function () {
        return __awaiter(this, void 0, void 0, function () {
            var _a;
            return __generator(this, function (_b) {
                switch (_b.label) {
                    case 0:
                        _a = this;
                        return [4 /*yield*/, this.restangular.all('points').getList().toPromise()];
                    case 1:
                        _a.points = _b.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    CreateComponent.prototype.onFileChange = function (event, type) {
        var _this = this;
        var file = event.target.files[0];
        var reader = new FileReader();
        reader.onload = function (ev) {
            if (type === 'logo') {
                _this.logo.path = _this.sanitizer.bypassSecurityTrustResourceUrl(ev.target.result);
                _this.logo.file = file;
            }
            else if (type === 'logo2') {
                _this.logo2.path = _this.sanitizer.bypassSecurityTrustResourceUrl(ev.target.result);
                _this.logo2.file = file;
            }
        };
        reader.readAsDataURL(file);
    };
    CreateComponent.prototype.onSubmit = function () {
        return __awaiter(this, void 0, void 0, function () {
            var journey, fdLogo, fdLogo_1;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        this.errors = [];
                        if (this.logo.file === null) {
                            this.errors.push({ message: "Picture is empty!" });
                            return [2 /*return*/];
                        }
                        return [4 /*yield*/, this.restangular.all('journeys').customPOST({ title_en: this.form.value.title_en, title_he: this.form.value.title_he, points: this.form.value.points }).toPromise()];
                    case 1:
                        journey = _a.sent();
                        fdLogo = new FormData();
                        fdLogo.append('journey_id', journey.id);
                        fdLogo.append('file', this.logo.file);
                        fdLogo.append('imagenumber', "1");
                        return [4 /*yield*/, this.restangular.all('media').customPOST(fdLogo).toPromise()];
                    case 2:
                        _a.sent();
                        if (!this.logo2.file) return [3 /*break*/, 4];
                        fdLogo_1 = new FormData();
                        fdLogo_1.append('journey_id', journey.id);
                        fdLogo_1.append('file', this.logo2.file);
                        fdLogo_1.append('imagenumber', "2");
                        return [4 /*yield*/, this.restangular.all('media').customPOST(fdLogo_1).toPromise()];
                    case 3:
                        _a.sent();
                        _a.label = 4;
                    case 4:
                        this.router.navigate(['journeys']);
                        return [2 /*return*/];
                }
            });
        });
    };
    CreateComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-create',
            changeDetection: __WEBPACK_IMPORTED_MODULE_0__angular_core__["ChangeDetectionStrategy"].Default,
            template: __webpack_require__("../../../../../src/app/journeys/create/create.component.html"),
            styles: [__webpack_require__("../../../../../src/app/journeys/create/create.component.css")],
        }),
        __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__angular_forms__["FormBuilder"] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__angular_forms__["FormBuilder"]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_2__angular_router__["c" /* Router */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__angular_router__["c" /* Router */]) === "function" && _b || Object, typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_3__angular_platform_browser__["DomSanitizer"] !== "undefined" && __WEBPACK_IMPORTED_MODULE_3__angular_platform_browser__["DomSanitizer"]) === "function" && _c || Object, typeof (_d = typeof __WEBPACK_IMPORTED_MODULE_4_ngx_restangular__["Restangular"] !== "undefined" && __WEBPACK_IMPORTED_MODULE_4_ngx_restangular__["Restangular"]) === "function" && _d || Object])
    ], CreateComponent);
    return CreateComponent;
    var _a, _b, _c, _d;
}());

//# sourceMappingURL=create.component.js.map

/***/ }),

/***/ "../../../../../src/app/journeys/edit/edit.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, ".wide-input {\r\n    width: 100% !important;\r\n    border: 1px solid grey;\r\n}\r\n\r\n.main-div {\r\n    padding: 5px 15px 15px 15px;\r\n    background-color: white;\r\n}\r\n\r\n.company-media {\r\n    display: -ms-flexbox;\r\n    display: flex;\r\n    -ms-flex-direction: row;\r\n        flex-direction: row;\r\n    -ms-flex-line-pack: center;\r\n        align-content: center;\r\n    -ms-flex-align: end;\r\n        align-items: flex-end;\r\n}\r\n\r\n.company-media img {\r\n    width: 30%;\r\n    border: 1px solid grey;\r\n}", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/journeys/edit/edit.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"main-div\">\r\n\r\n    <form class=\"form-horizontal\" novalidate [formGroup]=\"form\" *ngIf=\"journey\">\r\n\r\n        <div class=\"row\">\r\n\r\n            <div class=\"form-group col-md-6\">\r\n                <label class=\"col-form-label\">English title</label>\r\n                <input class=\"form-control\" formControlName=\"title_en\" type=\"text\">\r\n            </div>\r\n\r\n            <div class=\"form-group col-md-6\">\r\n                <label class=\"col-form-label\">Hebrew title</label>\r\n                <input class=\"form-control\" formControlName=\"title_he\" type=\"text\">\r\n            </div>\r\n\r\n        </div>\r\n\r\n        <div class=\"row mt-2\">\r\n            <div class=\"form-group col-md-12\">\r\n                <angular2-multiselect [data]=\"points\"\r\n                                      *ngIf=\"points\"\r\n                                      formControlName=\"points\"\r\n                                      [settings]=\"dropdownSettings\">\r\n                </angular2-multiselect>\r\n            </div>\r\n        </div>\r\n\r\n        <div class=\"row company-media\">\r\n\r\n            <div class=\"form-group col-md-6\">\r\n                <label class=\"col-form-label\">Choose picture</label>\r\n                <input class=\"wide-input\" name=\"logo\" (change)=\"onFileChange($event, 'logo')\" type=\"file\" accept=\"image/*\">\r\n            </div>\r\n\r\n            <div class=\"form-group col-md-6\">\r\n                <img [src]=\"logo.path\" *ngIf=\"logo.path\">\r\n                <img [src]=\"journey.image\" *ngIf=\"journey.image && !logo.path\">\r\n            </div>\r\n\r\n        </div>\r\n\r\n    <!-- new -->\r\n        <div class=\"row company-media\">\r\n\r\n            <div class=\"form-group col-md-6\">\r\n                <label class=\"col-form-label\">Choose inside picture</label>\r\n                <input class=\"wide-input\" name=\"logo2\" (change)=\"onFileChange($event, 'logo2')\" type=\"file\" accept=\"image/*\">\r\n            </div>\r\n\r\n            <div class=\"form-group col-md-6\">\r\n                <img [src]=\"logo2.path\" *ngIf=\"logo2.path\">\r\n                <img [src]=\"journey.imagetwo\" *ngIf=\"journey.imagetwo && !logo2.path\">\r\n            </div>\r\n\r\n        </div>\r\n        <!-- new -->\r\n\r\n\r\n        <div class=\"form-group col-md-6\" style=\"text-align: left !important;\">\r\n            <!--<button type=\"submit\" (click)=\"onSubmit()\" class=\"btn btn-primary\">Send</button>-->\r\n            <button type=\"submit\" (click)=\"onSubmit()\" class=\"btn btn-primary\" [disabled]=\"form.invalid\">Send</button>\r\n        </div>\r\n\r\n    </form>\r\n\r\n    <div class=\"row\" *ngIf=\"errors.length > 0\">\r\n        <div class=\"alert alert-warning\" role=\"alert\" *ngFor=\"let item of errors\">{{item.message}}</div>\r\n    </div>\r\n\r\n</div>"

/***/ }),

/***/ "../../../../../src/app/journeys/edit/edit.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return EditComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_forms__ = __webpack_require__("../../../forms/@angular/forms.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_router__ = __webpack_require__("../../../router/@angular/router.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_platform_browser__ = __webpack_require__("../../../platform-browser/@angular/platform-browser.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_ngx_restangular__ = __webpack_require__("../../../../ngx-restangular/dist/esm/src/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_ngx_restangular___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4_ngx_restangular__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = y[op[0] & 2 ? "return" : op[0] ? "throw" : "next"]) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [0, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};





var EditComponent = (function () {
    function EditComponent(fb, activatedRoute, router, sanitizer, restangular) {
        this.fb = fb;
        this.activatedRoute = activatedRoute;
        this.router = router;
        this.sanitizer = sanitizer;
        this.restangular = restangular;
        this.form = this.fb.group({
            title_en: new __WEBPACK_IMPORTED_MODULE_1__angular_forms__["FormControl"]('', __WEBPACK_IMPORTED_MODULE_1__angular_forms__["Validators"].required),
            title_he: new __WEBPACK_IMPORTED_MODULE_1__angular_forms__["FormControl"]('', __WEBPACK_IMPORTED_MODULE_1__angular_forms__["Validators"].required),
            points: new __WEBPACK_IMPORTED_MODULE_1__angular_forms__["FormControl"]([], __WEBPACK_IMPORTED_MODULE_1__angular_forms__["Validators"].required),
        });
        this.logo = { path: null, file: null };
        this.logo2 = { path: null, file: null };
        this.errors = [];
        this.points = [];
        this.dropdownSettings = {
            singleSelection: false,
            text: "Select points",
            selectAllText: 'Select All',
            unSelectAllText: 'UnSelect All'
        };
    }
    EditComponent.prototype.ngOnInit = function () {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            var _a;
            return __generator(this, function (_b) {
                switch (_b.label) {
                    case 0:
                        _a = this;
                        return [4 /*yield*/, this.restangular.all('points').getList().toPromise()];
                    case 1:
                        _a.points = _b.sent();
                        this.activatedRoute.params.subscribe(function (data) { return __awaiter(_this, void 0, void 0, function () {
                            var _a;
                            return __generator(this, function (_b) {
                                switch (_b.label) {
                                    case 0:
                                        _a = this;
                                        return [4 /*yield*/, this.restangular.one('journeys', data.id).get().toPromise()];
                                    case 1:
                                        _a.journey = _b.sent();
                                        this.form.setValue({ title_en: this.journey.title_en, title_he: this.journey.title_he, points: this.journey.points });
                                        return [2 /*return*/];
                                }
                            });
                        }); });
                        return [2 /*return*/];
                }
            });
        });
    };
    EditComponent.prototype.onFileChange = function (event, type) {
        var _this = this;
        var file = event.target.files[0];
        console.log("file", file);
        var reader = new FileReader();
        reader.onload = function (ev) {
            if (type === 'logo') {
                _this.logo.path = _this.sanitizer.bypassSecurityTrustResourceUrl(ev.target.result);
                _this.logo.file = file;
            }
            else if (type === 'logo2') {
                _this.logo2.path = _this.sanitizer.bypassSecurityTrustResourceUrl(ev.target.result);
                _this.logo2.file = file;
            }
        };
        reader.readAsDataURL(file);
    };
    EditComponent.prototype.onSubmit = function () {
        return __awaiter(this, void 0, void 0, function () {
            var fdLogo, fdLogo;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        this.errors = [];
                        if (this.journey.image == '' && this.logo.file === null) {
                            this.errors.push({ message: "Picture is empty!" });
                            return [2 /*return*/];
                        }
                        return [4 /*yield*/, this.restangular.one('journeys', this.journey.id).customPATCH({ title_en: this.form.value.title_en, title_he: this.form.value.title_he, points: this.form.value.points }).toPromise()];
                    case 1:
                        _a.sent();
                        if (!this.logo.file) return [3 /*break*/, 3];
                        fdLogo = new FormData();
                        fdLogo.append('journey_id', this.journey.id);
                        fdLogo.append('file', this.logo.file);
                        fdLogo.append('imagenumber', "1");
                        return [4 /*yield*/, this.restangular.all('media').customPOST(fdLogo).toPromise()];
                    case 2:
                        _a.sent();
                        _a.label = 3;
                    case 3:
                        if (!this.logo2.file) return [3 /*break*/, 5];
                        fdLogo = new FormData();
                        fdLogo.append('journey_id', this.journey.id);
                        fdLogo.append('file', this.logo2.file);
                        fdLogo.append('imagenumber', "2");
                        return [4 /*yield*/, this.restangular.all('media').customPOST(fdLogo).toPromise()];
                    case 4:
                        _a.sent();
                        _a.label = 5;
                    case 5:
                        this.router.navigate(['journeys']);
                        return [2 /*return*/];
                }
            });
        });
    };
    EditComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-edit',
            changeDetection: __WEBPACK_IMPORTED_MODULE_0__angular_core__["ChangeDetectionStrategy"].Default,
            template: __webpack_require__("../../../../../src/app/journeys/edit/edit.component.html"),
            styles: [__webpack_require__("../../../../../src/app/journeys/edit/edit.component.css")],
        }),
        __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__angular_forms__["FormBuilder"] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__angular_forms__["FormBuilder"]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_2__angular_router__["a" /* ActivatedRoute */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__angular_router__["a" /* ActivatedRoute */]) === "function" && _b || Object, typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_2__angular_router__["c" /* Router */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__angular_router__["c" /* Router */]) === "function" && _c || Object, typeof (_d = typeof __WEBPACK_IMPORTED_MODULE_3__angular_platform_browser__["DomSanitizer"] !== "undefined" && __WEBPACK_IMPORTED_MODULE_3__angular_platform_browser__["DomSanitizer"]) === "function" && _d || Object, typeof (_e = typeof __WEBPACK_IMPORTED_MODULE_4_ngx_restangular__["Restangular"] !== "undefined" && __WEBPACK_IMPORTED_MODULE_4_ngx_restangular__["Restangular"]) === "function" && _e || Object])
    ], EditComponent);
    return EditComponent;
    var _a, _b, _c, _d, _e;
}());

//# sourceMappingURL=edit.component.js.map

/***/ }),

/***/ "../../../../../src/app/journeys/index/index.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, ".badge-point {\r\n    margin: 3px;\r\n    cursor: move;\r\n}", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/journeys/index/index.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"row\">\r\n  <div class=\"col-md-10\">\r\n    <input type=\"text\" class=\"form-control mb-3\" placeholder=\"Type to filter the title column...\" required (keyup)='updateFilter($event)'>\r\n  </div>\r\n  <div class=\"col-md-2\">\r\n    <button type=\"button\" class=\"btn btn-info\" [routerLink]=\"['/journeys/create']\">New Journey</button>\r\n  </div>\r\n</div>\r\n<ngx-datatable\r\n        [headerHeight]=\"40\"\r\n        [footerHeight]=\"'falsey'\"\r\n        [rowHeight]=\"'auto'\"\r\n        [scrollbarH]=\"true\"\r\n        [columnMode]=\"'force'\"\r\n        [rows]=\"rows\">\r\n\r\n  <!-- Column Templates -->\r\n\r\n  <ngx-datatable-column name=\"Image\" [maxWidth]=\"70\">\r\n    <ng-template let-row=\"row\" ngx-datatable-cell-template>\r\n      <img [src]=\"row.image\" *ngIf=\"row.image\" height=\"50\">\r\n    </ng-template>\r\n  </ngx-datatable-column>\r\n\r\n  <ngx-datatable-column name=\"Inside Image\" [maxWidth]=\"130\">\r\n    <ng-template let-row=\"row\" ngx-datatable-cell-template>\r\n      <img [src]=\"row.imagetwo\" *ngIf=\"row.imagetwo\" height=\"50\">\r\n    </ng-template>\r\n  </ngx-datatable-column>\r\n\r\n  <ngx-datatable-column name=\"English title\" [maxWidth]=\"200\">\r\n    <ng-template let-row=\"row\" ngx-datatable-cell-template>\r\n      <strong>{{row.title_en}}</strong>\r\n    </ng-template>\r\n  </ngx-datatable-column>\r\n\r\n  <ngx-datatable-column name=\"Hebrew title\" [maxWidth]=\"200\">\r\n    <ng-template let-row=\"row\" ngx-datatable-cell-template>\r\n      <strong>{{row.title_he}}</strong>\r\n    </ng-template>\r\n  </ngx-datatable-column>\r\n\r\n  <ngx-datatable-column name=\"Points\">\r\n    <ng-template let-row=\"row\" ngx-datatable-cell-template>\r\n      <div [sortablejs]=\"row.points\" [sortablejsOptions]=\"options[row.id]\" *ngIf=\"options[row.id]\">\r\n        <span class=\"badge badge-primary badge-point\" *ngFor=\"let point of row.points\">{{ point.title }}</span>\r\n      </div>\r\n    </ng-template>\r\n  </ngx-datatable-column>\r\n\r\n  <ngx-datatable-column name=\"Options\" [sortable]=\"false\" [maxWidth]=\"200\">\r\n    <ng-template let-row=\"row\" ngx-datatable-cell-template>\r\n      <button type=\"button\" class=\"btn btn-warning\" [routerLink]=\"['/journeys/edit/' + row.id]\">Edit</button>\r\n      <button type=\"button\" class=\"btn btn-danger\" (click)=\"openDeleteModal(content, row)\">Delete</button>\r\n    </ng-template>\r\n  </ngx-datatable-column>\r\n\r\n</ngx-datatable>\r\n\r\n<ng-template ngbModalContainer></ng-template>\r\n<ng-template #content let-c=\"close\" let-d=\"dismiss\">\r\n  <div class=\"modal-header\">\r\n    <h6 class=\"modal-title text-uppercase\">Delete point</h6>\r\n    <button type=\"button\" class=\"close\" aria-label=\"Close\" (click)=\"d()\">\r\n      <span aria-hidden=\"true\">&times;</span>\r\n    </button>\r\n  </div>\r\n  <div class=\"modal-body\">Are you sure that you want to delete this journey? It will delete all content that is connected to this journey in all journeys.</div>\r\n  <div class=\"modal-footer\">\r\n    <button type=\"button\" class=\"btn btn-secondary\" (click)=\"c()\">Close</button>\r\n    <button type=\"button\" class=\"btn btn-primary\" (click)=\"deleteItem()\">Delete</button>\r\n  </div>\r\n</ng-template>"

/***/ }),

/***/ "../../../../../src/app/journeys/index/index.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return IndexComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__ng_bootstrap_ng_bootstrap__ = __webpack_require__("../../../../@ng-bootstrap/ng-bootstrap/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ngx_restangular__ = __webpack_require__("../../../../ngx-restangular/dist/esm/src/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ngx_restangular___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_ngx_restangular__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_forms__ = __webpack_require__("../../../forms/@angular/forms.es5.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = y[op[0] & 2 ? "return" : op[0] ? "throw" : "next"]) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [0, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};




var IndexComponent = (function () {
    function IndexComponent(restangular, modalService, fb) {
        this.restangular = restangular;
        this.modalService = modalService;
        this.fb = fb;
        this.form = this.fb.group({ title: new __WEBPACK_IMPORTED_MODULE_3__angular_forms__["FormControl"]('', __WEBPACK_IMPORTED_MODULE_3__angular_forms__["Validators"].required), });
        this.dropdownSettings = {
            text: "Select points",
            disabled: true
        };
        this.options = [];
    }
    IndexComponent.prototype.ngOnInit = function () {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            var _a, _loop_1, this_1, _i, _b, journey;
            return __generator(this, function (_c) {
                switch (_c.label) {
                    case 0:
                        _a = this;
                        return [4 /*yield*/, this.restangular.all('journeys').getList().toPromise()];
                    case 1:
                        _a.rows = _c.sent();
                        this.journeys = this.rows;
                        _loop_1 = function (journey) {
                            this_1.options[journey.id] = {
                                group: journey.id,
                                onUpdate: function (event) { return __awaiter(_this, void 0, void 0, function () {
                                    return __generator(this, function (_a) {
                                        switch (_a.label) {
                                            case 0: return [4 /*yield*/, this.restangular.one('journeys', journey.id).customPATCH({
                                                    order: this.journeys.find(function (j) { return j.id == journey.id; }).points.map(function (p) { return p.id; }),
                                                }).toPromise()];
                                            case 1:
                                                _a.sent();
                                                return [2 /*return*/];
                                        }
                                    });
                                }); }
                            };
                        };
                        this_1 = this;
                        for (_i = 0, _b = this.journeys; _i < _b.length; _i++) {
                            journey = _b[_i];
                            _loop_1(journey);
                        }
                        return [2 /*return*/];
                }
            });
        });
    };
    IndexComponent.prototype.updateFilter = function (event) {
        var val = event.target.value.toLowerCase();
        var temp = this.journeys.filter(function (d) {
            return d.title_en && d.title_en.toLowerCase().indexOf(val) !== -1 || d.title_he && d.title_he.toLowerCase().indexOf(val) !== -1 || !val;
        });
        this.rows = temp;
    };
    IndexComponent.prototype.deleteItem = function () {
        return __awaiter(this, void 0, void 0, function () {
            var _a;
            return __generator(this, function (_b) {
                switch (_b.label) {
                    case 0: return [4 /*yield*/, this.itemToDelete.remove().toPromise()];
                    case 1:
                        _b.sent();
                        this.deleteModal.close();
                        _a = this;
                        return [4 /*yield*/, this.restangular.all('journeys').getList().toPromise()];
                    case 2:
                        _a.rows = _b.sent();
                        this.journeys = this.rows;
                        return [2 /*return*/];
                }
            });
        });
    };
    IndexComponent.prototype.openDeleteModal = function (content, item) {
        this.deleteModal = this.modalService.open(content);
        this.itemToDelete = item;
    };
    IndexComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-index',
            template: __webpack_require__("../../../../../src/app/journeys/index/index.component.html"),
            styles: [__webpack_require__("../../../../../src/app/journeys/index/index.component.css")]
        }),
        __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_2_ngx_restangular__["Restangular"] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2_ngx_restangular__["Restangular"]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_1__ng_bootstrap_ng_bootstrap__["a" /* NgbModal */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__ng_bootstrap_ng_bootstrap__["a" /* NgbModal */]) === "function" && _b || Object, typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_3__angular_forms__["FormBuilder"] !== "undefined" && __WEBPACK_IMPORTED_MODULE_3__angular_forms__["FormBuilder"]) === "function" && _c || Object])
    ], IndexComponent);
    return IndexComponent;
    var _a, _b, _c;
}());

//# sourceMappingURL=index.component.js.map

/***/ }),

/***/ "../../../../../src/app/journeys/journeys.module.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "JourneysModule", function() { return JourneysModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_common__ = __webpack_require__("../../../common/@angular/common.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__("../../../router/@angular/router.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__swimlane_ngx_datatable__ = __webpack_require__("../../../../@swimlane/ngx-datatable/release/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__swimlane_ngx_datatable___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3__swimlane_ngx_datatable__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__index_index_component__ = __webpack_require__("../../../../../src/app/journeys/index/index.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_ng2_file_upload_ng2_file_upload__ = __webpack_require__("../../../../ng2-file-upload/ng2-file-upload.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_ng2_file_upload_ng2_file_upload___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_5_ng2_file_upload_ng2_file_upload__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_angular_tree_component__ = __webpack_require__("../../../../angular-tree-component/dist/angular-tree-component.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7_ng2_validation__ = __webpack_require__("../../../../ng2-validation/dist/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7_ng2_validation___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_7_ng2_validation__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__angular_forms__ = __webpack_require__("../../../forms/@angular/forms.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__ng_bootstrap_ng_bootstrap__ = __webpack_require__("../../../../@ng-bootstrap/ng-bootstrap/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10_angular2_text_mask__ = __webpack_require__("../../../../angular2-text-mask/dist/angular2TextMask.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10_angular2_text_mask___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_10_angular2_text_mask__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11_ngx_loading__ = __webpack_require__("../../../../ngx-loading/ngx-loading/ngx-loading.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__create_create_component__ = __webpack_require__("../../../../../src/app/journeys/create/create.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13__edit_edit_component__ = __webpack_require__("../../../../../src/app/journeys/edit/edit.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_14_angular2_multiselect_dropdown__ = __webpack_require__("../../../../angular2-multiselect-dropdown/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_15_angular2_moment__ = __webpack_require__("../../../../angular2-moment/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_15_angular2_moment___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_15_angular2_moment__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_16__journeys_routing__ = __webpack_require__("../../../../../src/app/journeys/journeys.routing.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_17_angular_sortablejs__ = __webpack_require__("../../../../angular-sortablejs/dist/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_17_angular_sortablejs___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_17_angular_sortablejs__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};


















var JourneysModule = (function () {
    function JourneysModule() {
    }
    JourneysModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_2__angular_core__["NgModule"])({
            imports: [__WEBPACK_IMPORTED_MODULE_0__angular_common__["CommonModule"],
                __WEBPACK_IMPORTED_MODULE_1__angular_router__["d" /* RouterModule */].forChild(__WEBPACK_IMPORTED_MODULE_16__journeys_routing__["a" /* JourneysRoutes */]),
                __WEBPACK_IMPORTED_MODULE_3__swimlane_ngx_datatable__["NgxDatatableModule"],
                __WEBPACK_IMPORTED_MODULE_8__angular_forms__["FormsModule"],
                __WEBPACK_IMPORTED_MODULE_8__angular_forms__["ReactiveFormsModule"],
                __WEBPACK_IMPORTED_MODULE_9__ng_bootstrap_ng_bootstrap__["c" /* NgbProgressbarModule */],
                __WEBPACK_IMPORTED_MODULE_7_ng2_validation__["CustomFormsModule"],
                __WEBPACK_IMPORTED_MODULE_6_angular_tree_component__["a" /* TreeModule */],
                __WEBPACK_IMPORTED_MODULE_5_ng2_file_upload_ng2_file_upload__["FileUploadModule"],
                __WEBPACK_IMPORTED_MODULE_11_ngx_loading__["a" /* LoadingModule */],
                __WEBPACK_IMPORTED_MODULE_10_angular2_text_mask__["TextMaskModule"],
                __WEBPACK_IMPORTED_MODULE_14_angular2_multiselect_dropdown__["a" /* AngularMultiSelectModule */],
                __WEBPACK_IMPORTED_MODULE_15_angular2_moment__["MomentModule"],
                __WEBPACK_IMPORTED_MODULE_17_angular_sortablejs__["SortablejsModule"],
            ],
            declarations: [__WEBPACK_IMPORTED_MODULE_4__index_index_component__["a" /* IndexComponent */], __WEBPACK_IMPORTED_MODULE_12__create_create_component__["a" /* CreateComponent */], __WEBPACK_IMPORTED_MODULE_13__edit_edit_component__["a" /* EditComponent */]]
        })
    ], JourneysModule);
    return JourneysModule;
}());

//# sourceMappingURL=journeys.module.js.map

/***/ }),

/***/ "../../../../../src/app/journeys/journeys.routing.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return JourneysRoutes; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__index_index_component__ = __webpack_require__("../../../../../src/app/journeys/index/index.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__create_create_component__ = __webpack_require__("../../../../../src/app/journeys/create/create.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__edit_edit_component__ = __webpack_require__("../../../../../src/app/journeys/edit/edit.component.ts");



var JourneysRoutes = [{
        path: '',
        children: [
            {
                path: '',
                component: __WEBPACK_IMPORTED_MODULE_0__index_index_component__["a" /* IndexComponent */],
                data: { heading: 'Journeys' },
            },
            {
                path: 'create',
                component: __WEBPACK_IMPORTED_MODULE_1__create_create_component__["a" /* CreateComponent */],
                data: { heading: 'New journey' },
            },
            {
                path: 'edit/:id',
                component: __WEBPACK_IMPORTED_MODULE_2__edit_edit_component__["a" /* EditComponent */],
                data: { heading: 'Edit journey' },
            }
        ]
    }];
//# sourceMappingURL=journeys.routing.js.map

/***/ })

});
//# sourceMappingURL=journeys.module.chunk.js.map