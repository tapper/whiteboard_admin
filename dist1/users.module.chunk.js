webpackJsonp(["users.module"],{

/***/ "../../../../../src/app/users_journeys_new/index/index.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, ".buttonsBar\r\n{\r\n    width: 100%;\r\n    overflow: hidden;\r\n    margin-top: 15px;\r\n}\r\n\r\n.buttonsBarLeft\r\n{\r\n    width: 40%;\r\n    float: left;\r\n    margin-left: 5%;\r\n}\r\n\r\n.buttonsBarRight\r\n{\r\n    width: 40%;\r\n    float: right;\r\n    margin-right: 5%;\r\n}\r\n\r\n.buttonsBarButton\r\n{\r\n    width: 100%;\r\n    background-color:#6fb43f;\r\n}\r\n\r\n.table-responsive\r\n{\r\n    overflow-x: scroll;\r\n}\r\n\r\n.trTable\r\n{\r\n    width:200px !important;\r\n    overflow-x: scroll;\r\n}\r\n\r\ntd\r\n{\r\n    border: none;\r\n}\r\n\r\ntr\r\n{\r\n    min-width: 100%;\r\n}\r\n.divRow\r\n{\r\n    border-bottom:1px solid #cccccc;\r\n}\r\ntbody\r\n{\r\nwidth: 200px !important;\r\n}", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/users_journeys_new/index/index.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"row\">\r\n    <div class=\"col-md-1\" align=\"center\">\r\n        <button type=\"button\" class=\"btn btn-info\" (click)=\"showDetailsFun()\">Show details</button>\r\n    </div>\r\n  <div class=\"col-md-7\">\r\n    <input type=\"text\" class=\"form-control mb-3\" placeholder=\"Type to filter the email column...\" required (keyup)='updateFilter($event)'>\r\n  </div>\r\n  <div class=\"col-md-4\">\r\n      <button type=\"button\" class=\"btn btn-info\" (click)=\"exportExcel('en')\">Export as Excel (english)</button>\r\n      <button type=\"button\" class=\"btn btn-info\" (click)=\"exportExcel('he')\">Export as Excel (hebrew)</button>\r\n  </div>\r\n</div>\r\n\r\n\r\n\r\n<div class=\"buttonsBar\" align=\"center\">\r\n    <button  *ngFor=\"let row of jurneysArray; let i = index\" [ngClass]=\"[JurneyType == row.journey_id ? 'btn-success btn-md' : 'btn-primary btn-md']\" (click)=\"ChangeJurneyType(row.journey_id)\"> {{row.title}}</button>\r\n</div>\r\n\r\n\r\n<div class=\"table-responsive\" [style.width.px]=\"TableWidth\" >\r\n  <table class=\"table table hover\" id=\"basic-usage\" border=\"0\"  dir=\"ltr\">\r\n    <tbody>\r\n\r\n        <tr *ngFor=\"let row of rows\"  [hidden]=\"!row.count\" class=\"trTable\" >\r\n            <div class=\"divRow\">\r\n           <td *ngIf=\"showDetails == 1\" (click)=\"showRow(row)\" style=\"min-width:180px;\">{{row.created_at}}</td>\r\n            <td *ngIf=\"showDetails == 1\"(click)=\"showRow(row)\" style=\"min-width:200px;  \">{{row.user_data.email}}</td>\r\n            <td *ngIf=\"showDetails == 1\" (click)=\"showRow(row)\" style=\"min-width:200px; \">{{row.user_data.name}}</td>\r\n            <!--<td (click)=\"showRow(row)\">{{row.user_data.created_at}}</td>-->\r\n\r\n            <td style=\"min-width:100px;\" *ngFor=\"let item of row.touchpoints\" >\r\n                {{item.title}}\r\n                <div *ngIf=\"!row.isHidden\" >\r\n                    <div *ngFor=\"let media of item.media\">\r\n                        <div *ngIf=\"media.type == 'image'\">\r\n                            <button type=\"button\" class=\"btn btn-clear mb-1\" (click)=\"openImageModal(content, media)\">\r\n                                <img [src]=\"media.fullUrl\" width=\"90\">\r\n                            </button>\r\n                        </div>\r\n                        <div *ngIf=\"media.type == 'audio'\">\r\n                            <div class=\"mb-1\"><audio [src]=\"media.fullUrl\" controls style=\"width: 250px\"></audio></div>\r\n                        </div>\r\n                        <div *ngIf=\"media.type == 'video'\">\r\n                            <div class=\"mb-1\"><video [src]=\"media.fullUrl\" controls height=\"100\"></video></div>\r\n                        </div>\r\n                        <!--<div *ngIf=\"media.type == 'text'\">-->\r\n                            <!--<div>{{media.content}}</div>-->\r\n                        <!--</div>-->\r\n                    </div>\r\n\r\n                  <div *ngFor=\"let text of item.texts\">\r\n                    <div *ngIf=\"text.type == 'text'\">\r\n                      <div>{{text.content}}</div>\r\n                    </div>\r\n                  </div>\r\n\r\n\r\n                </div>\r\n\r\n            </td>\r\n            </div>\r\n        </tr>\r\n\r\n\r\n    <!--<tr *ngFor=\"let row of rows\"  [hidden]=\"!row.count\" style=\"min-width:100% !important;\" >-->\r\n      <!--<td (click)=\"showRow(row)\">{{row.user_data.email}}</td>-->\r\n      <!--<td (click)=\"showRow(row)\">{{row.user_data.name}}</td>-->\r\n      <!--&lt;!&ndash;<td (click)=\"showRow(row)\">{{row.user_data.created_at}}</td>&ndash;&gt;-->\r\n\r\n      <!--<td *ngFor=\"let item of row.touchpoint.content\">-->\r\n        <!--{{row.touchpoint.title}}-->\r\n        <!--<div *ngIf=\"!row.isHidden\" >-->\r\n          <!--<div *ngFor=\"let media of row.touchpoint.content\">-->\r\n            <!--<div *ngIf=\"media.type == 'image'\">-->\r\n              <!--<button type=\"button\" class=\"btn btn-clear mb-1\" (click)=\"openImageModal(content, media)\">-->\r\n                <!--<img [src]=\"media.fullUrl\" width=\"90\">-->\r\n              <!--</button>-->\r\n            <!--</div>-->\r\n            <!--<div *ngIf=\"media.type == 'audio'\">-->\r\n              <!--<div class=\"mb-1\"><audio [src]=\"media.fullUrl\" controls style=\"width: 250px\"></audio></div>-->\r\n            <!--</div>-->\r\n            <!--<div *ngIf=\"media.type == 'video'\">-->\r\n              <!--<div class=\"mb-1\"><video [src]=\"media.fullUrl\" controls height=\"100\"></video></div>-->\r\n            <!--</div>-->\r\n            <!--<div *ngIf=\"media.type == 'text'\">-->\r\n              <!--<div>{{media.content}}</div>-->\r\n            <!--</div>-->\r\n          <!--</div>-->\r\n        <!--</div>-->\r\n      <!--</td>-->\r\n\r\n    <!--</tr>-->\r\n\r\n\r\n\r\n    </tbody>\r\n  </table>\r\n</div>\r\n\r\n\r\n\r\n\r\n\r\n<ng-template #content let-c=\"close\">\r\n  <div class=\"modal-body\">\r\n    <div><img [src]=\"selectedImage.fullUrl\" style=\"width: 100%;\"></div>\r\n  </div>\r\n  <div class=\"modal-footer\">\r\n    <button type=\"button\" class=\"btn btn-secondary\" (click)=\"c()\">Close</button>\r\n  </div>\r\n</ng-template>\r\n"

/***/ }),

/***/ "../../../../../src/app/users_journeys_new/index/index.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return IndexComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__ng_bootstrap_ng_bootstrap__ = __webpack_require__("../../../../@ng-bootstrap/ng-bootstrap/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ngx_restangular__ = __webpack_require__("../../../../ngx-restangular/dist/esm/src/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ngx_restangular___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_ngx_restangular__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_forms__ = __webpack_require__("../../../forms/@angular/forms.es5.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = y[op[0] & 2 ? "return" : op[0] ? "throw" : "next"]) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [0, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};




var IndexComponent = (function () {
    function IndexComponent(restangular, modalService, fb) {
        this.restangular = restangular;
        this.modalService = modalService;
        this.fb = fb;
        this.JurneyType = 1;
        this.TableWidth = window.innerWidth * 0.85;
        this.editMode = false;
        this.editRow = null;
        this.fieldsToEdit = { name: '', phone: '' };
        this.showDetails = 1;
        this.form = this.fb.group({
            name: new __WEBPACK_IMPORTED_MODULE_3__angular_forms__["FormControl"]('', __WEBPACK_IMPORTED_MODULE_3__angular_forms__["Validators"].required),
            email: new __WEBPACK_IMPORTED_MODULE_3__angular_forms__["FormControl"]('', __WEBPACK_IMPORTED_MODULE_3__angular_forms__["Validators"].required),
            phone: new __WEBPACK_IMPORTED_MODULE_3__angular_forms__["FormControl"]('', __WEBPACK_IMPORTED_MODULE_3__angular_forms__["Validators"].required),
        });
    }
    IndexComponent.prototype.ngOnInit = function () {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                // this.rows = await this.restangular.all('getUsersByJourneyNew').getList().toPromise();
                this.getJurneyNames();
                this.getData(this.JurneyType);
                return [2 /*return*/];
            });
        });
    };
    IndexComponent.prototype.getJurneyNames = function () {
        return __awaiter(this, void 0, void 0, function () {
            var _a;
            return __generator(this, function (_b) {
                switch (_b.label) {
                    case 0:
                        _a = this;
                        return [4 /*yield*/, this.restangular.all('getJurneyNames').getList().toPromise()];
                    case 1:
                        _a.jurneysArray = _b.sent();
                        console.log("jurneysArray", this.jurneysArray);
                        return [2 /*return*/];
                }
            });
        });
    };
    IndexComponent.prototype.getData = function (type) {
        return __awaiter(this, void 0, void 0, function () {
            var data, _a;
            return __generator(this, function (_b) {
                switch (_b.label) {
                    case 0:
                        data = new FormData();
                        data.append('JurneyType', type);
                        _a = this;
                        return [4 /*yield*/, this.restangular.all('getUsersByJourneyNew').customPOST(data).toPromise()];
                    case 1:
                        _a.rows = _b.sent();
                        // this.rows = await this.restangular.all('getUsersByJourneyNew2').customPOST(data).toPromise();
                        this.rows1 = this.rows; //await this.restangular.all('getUsersByJourneyNew').customPOST(data).toPromise();
                        console.log("TouchPoints : ", this.rows);
                        return [2 /*return*/];
                }
            });
        });
    };
    IndexComponent.prototype.ChangeJurneyType = function (type) {
        this.JurneyType = type;
        this.getData(this.JurneyType);
    };
    IndexComponent.prototype.showRow = function (row) {
        if (row.isHidden)
            row.isHidden = false;
        else
            row.isHidden = true;
    };
    IndexComponent.prototype.exportExcel = function (locale) {
        window.open("http://whiteboard.kartisim.co.il/v1/getUsersByJourneyExcel?id=" + this.JurneyType + "&locale=" + locale, "_self");
    };
    IndexComponent.prototype.openImageModal = function (content, image) {
        this.selectedImage = image;
        this.imageModal = this.modalService.open(content);
    };
    IndexComponent.prototype.updateFilter = function (event) {
        var val = event.target.value.toLowerCase();
        // filter our data
        this.rows = this.rows1;
        var temp = this.rows.filter(function (d) {
            return d.user_data.email && d.user_data.email.toLowerCase().indexOf(val) !== -1;
        });
        // update the rows
        this.rows = temp;
    };
    IndexComponent.prototype.deleteItem = function () {
        return __awaiter(this, void 0, void 0, function () {
            var _a;
            return __generator(this, function (_b) {
                switch (_b.label) {
                    case 0: return [4 /*yield*/, this.itemToDelete.remove().toPromise()];
                    case 1:
                        _b.sent();
                        this.deleteModal.close();
                        _a = this;
                        return [4 /*yield*/, this.restangular.all('users').getList().toPromise()];
                    case 2:
                        _a.rows = _b.sent();
                        this.users = this.rows;
                        return [2 /*return*/];
                }
            });
        });
    };
    IndexComponent.prototype.openDeleteModal = function (content, item) {
        this.deleteModal = this.modalService.open(content);
        this.itemToDelete = item;
    };
    IndexComponent.prototype.switchToEditMode = function (item) {
        this.editMode = true;
        this.editRow = item;
        this.fieldsToEdit.name = item.name;
        this.fieldsToEdit.phone = item.phone;
    };
    IndexComponent.prototype.editItem = function () {
        return __awaiter(this, void 0, void 0, function () {
            var _a;
            return __generator(this, function (_b) {
                switch (_b.label) {
                    case 0:
                        this.editRow.name = this.fieldsToEdit.name;
                        this.editRow.phone = this.fieldsToEdit.phone;
                        return [4 /*yield*/, this.editRow.patch().toPromise()];
                    case 1:
                        _b.sent();
                        _a = this;
                        return [4 /*yield*/, this.restangular.all('users').getList().toPromise()];
                    case 2:
                        _a.rows = _b.sent();
                        this.users = this.rows;
                        this.cancelEdit();
                        return [2 /*return*/];
                }
            });
        });
    };
    IndexComponent.prototype.cancelEdit = function () {
        this.editMode = false;
        this.ngOnInit();
    };
    IndexComponent.prototype.openCreateModal = function (content) {
        this.createModal = this.modalService.open(content);
    };
    IndexComponent.prototype.createItem = function () {
        return __awaiter(this, void 0, void 0, function () {
            var item, _a;
            return __generator(this, function (_b) {
                switch (_b.label) {
                    case 0:
                        item = this.restangular.restangularizeElement('', { name: this.form.value.name, email: this.form.value.email, phone: this.form.value.phone }, 'users');
                        return [4 /*yield*/, item.save().toPromise()];
                    case 1:
                        _b.sent();
                        this.createModal.close();
                        _a = this;
                        return [4 /*yield*/, this.restangular.all('users').getList().toPromise()];
                    case 2:
                        _a.rows = _b.sent();
                        this.users = this.rows;
                        return [2 /*return*/];
                }
            });
        });
    };
    IndexComponent.prototype.showDetailsFun = function () {
        console.log(this.showDetails);
        if (this.showDetails == 1)
            this.showDetails = 0;
        else
            this.showDetails = 1;
    };
    IndexComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-index',
            template: __webpack_require__("../../../../../src/app/users_journeys_new/index/index.component.html"),
            styles: [__webpack_require__("../../../../../src/app/users_journeys_new/index/index.component.css")]
        }),
        __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_2_ngx_restangular__["Restangular"] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2_ngx_restangular__["Restangular"]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_1__ng_bootstrap_ng_bootstrap__["a" /* NgbModal */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__ng_bootstrap_ng_bootstrap__["a" /* NgbModal */]) === "function" && _b || Object, typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_3__angular_forms__["FormBuilder"] !== "undefined" && __WEBPACK_IMPORTED_MODULE_3__angular_forms__["FormBuilder"]) === "function" && _c || Object])
    ], IndexComponent);
    return IndexComponent;
    var _a, _b, _c;
}());

//# sourceMappingURL=index.component.js.map

/***/ }),

/***/ "../../../../../src/app/users_journeys_new/users.module.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UsersModule", function() { return UsersModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_common__ = __webpack_require__("../../../common/@angular/common.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__("../../../router/@angular/router.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__swimlane_ngx_datatable__ = __webpack_require__("../../../../@swimlane/ngx-datatable/release/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__swimlane_ngx_datatable___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3__swimlane_ngx_datatable__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__index_index_component__ = __webpack_require__("../../../../../src/app/users_journeys_new/index/index.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_ng2_file_upload_ng2_file_upload__ = __webpack_require__("../../../../ng2-file-upload/ng2-file-upload.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_ng2_file_upload_ng2_file_upload___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_5_ng2_file_upload_ng2_file_upload__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_angular_tree_component__ = __webpack_require__("../../../../angular-tree-component/dist/angular-tree-component.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7_ng2_validation__ = __webpack_require__("../../../../ng2-validation/dist/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7_ng2_validation___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_7_ng2_validation__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__angular_forms__ = __webpack_require__("../../../forms/@angular/forms.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__ng_bootstrap_ng_bootstrap__ = __webpack_require__("../../../../@ng-bootstrap/ng-bootstrap/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10_angular2_text_mask__ = __webpack_require__("../../../../angular2-text-mask/dist/angular2TextMask.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10_angular2_text_mask___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_10_angular2_text_mask__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11_ngx_loading__ = __webpack_require__("../../../../ngx-loading/ngx-loading/ngx-loading.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12_angular2_multiselect_dropdown__ = __webpack_require__("../../../../angular2-multiselect-dropdown/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13_angular2_moment__ = __webpack_require__("../../../../angular2-moment/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13_angular2_moment___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_13_angular2_moment__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_14__users_routing__ = __webpack_require__("../../../../../src/app/users_journeys_new/users.routing.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};















var UsersModule = (function () {
    function UsersModule() {
    }
    UsersModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_2__angular_core__["NgModule"])({
            imports: [__WEBPACK_IMPORTED_MODULE_0__angular_common__["CommonModule"],
                __WEBPACK_IMPORTED_MODULE_1__angular_router__["d" /* RouterModule */].forChild(__WEBPACK_IMPORTED_MODULE_14__users_routing__["a" /* UsersRoutes */]),
                __WEBPACK_IMPORTED_MODULE_3__swimlane_ngx_datatable__["NgxDatatableModule"],
                __WEBPACK_IMPORTED_MODULE_8__angular_forms__["FormsModule"],
                __WEBPACK_IMPORTED_MODULE_8__angular_forms__["ReactiveFormsModule"],
                __WEBPACK_IMPORTED_MODULE_9__ng_bootstrap_ng_bootstrap__["c" /* NgbProgressbarModule */],
                __WEBPACK_IMPORTED_MODULE_7_ng2_validation__["CustomFormsModule"],
                __WEBPACK_IMPORTED_MODULE_6_angular_tree_component__["a" /* TreeModule */],
                __WEBPACK_IMPORTED_MODULE_5_ng2_file_upload_ng2_file_upload__["FileUploadModule"],
                __WEBPACK_IMPORTED_MODULE_11_ngx_loading__["a" /* LoadingModule */],
                __WEBPACK_IMPORTED_MODULE_10_angular2_text_mask__["TextMaskModule"],
                __WEBPACK_IMPORTED_MODULE_12_angular2_multiselect_dropdown__["a" /* AngularMultiSelectModule */],
                __WEBPACK_IMPORTED_MODULE_13_angular2_moment__["MomentModule"]
            ],
            declarations: [__WEBPACK_IMPORTED_MODULE_4__index_index_component__["a" /* IndexComponent */]]
        })
    ], UsersModule);
    return UsersModule;
}());

//# sourceMappingURL=users.module.js.map

/***/ }),

/***/ "../../../../../src/app/users_journeys_new/users.routing.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return UsersRoutes; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__index_index_component__ = __webpack_require__("../../../../../src/app/users_journeys_new/index/index.component.ts");

var UsersRoutes = [{
        path: '',
        children: [
            {
                path: '',
                component: __WEBPACK_IMPORTED_MODULE_0__index_index_component__["a" /* IndexComponent */],
                data: { heading: 'User Journeys New' },
            }
        ]
    }];
//# sourceMappingURL=users.routing.js.map

/***/ })

});
//# sourceMappingURL=users.module.chunk.js.map